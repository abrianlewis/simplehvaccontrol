;/////////////////////////////////////////////////////////////////////////////////
;// Code Generator: BoostC Compiler - http://www.sourceboost.com
;// Version       : 7.22
;// License Type  : Full License
;// Limitations   : PIC12,PIC16 max code size:Unlimited, max RAM banks:Unlimited, Non commercial use only
;/////////////////////////////////////////////////////////////////////////////////

	include "P16F648A.inc"
; Heap block 0, size:95 (0x000000A0 - 0x000000FE)
__HEAP_BLOCK0_BANK               EQU	0x00000001
__HEAP_BLOCK0_START_OFFSET       EQU	0x00000020
__HEAP_BLOCK0_END_OFFSET         EQU	0x0000007E
; Heap block 1, size:80 (0x00000120 - 0x0000016F)
__HEAP_BLOCK1_BANK               EQU	0x00000002
__HEAP_BLOCK1_START_OFFSET       EQU	0x00000020
__HEAP_BLOCK1_END_OFFSET         EQU	0x0000006F
; Heap block 2, size:10 (0x00000066 - 0x0000006F)
__HEAP_BLOCK2_BANK               EQU	0x00000000
__HEAP_BLOCK2_START_OFFSET       EQU	0x00000066
__HEAP_BLOCK2_END_OFFSET         EQU	0x0000006F
; Heap block 3, size:0 (0x00000000 - 0x00000000)
__HEAP_BLOCK3_BANK               EQU	0x00000000
__HEAP_BLOCK3_START_OFFSET       EQU	0x00000000
__HEAP_BLOCK3_END_OFFSET         EQU	0x00000000
gbl_status                       EQU	0x00000003 ; bytes:1
__mul_16u__0000C_arg_a           EQU	0x00000052 ; bytes:2
__mul_16u__0000C_arg_b           EQU	0x00000054 ; bytes:2
CompTempVarRet378                EQU	0x0000005C ; bytes:2
__mul_16u__0000C_1_i             EQU	0x00000059 ; bytes:1
__mul_16u__0000C_1_t             EQU	0x0000005A ; bytes:2
gbl_indf                         EQU	0x00000000 ; bytes:1
gbl_tmr0                         EQU	0x00000001 ; bytes:1
gbl_pcl                          EQU	0x00000002 ; bytes:1
gbl_fsr                          EQU	0x00000004 ; bytes:1
gbl_porta                        EQU	0x00000005 ; bytes:1
gbl_portb                        EQU	0x00000006 ; bytes:1
gbl_pclath                       EQU	0x0000000A ; bytes:1
gbl_intcon                       EQU	0x0000000B ; bytes:1
gbl_pir1                         EQU	0x0000000C ; bytes:1
gbl_tmr1l                        EQU	0x0000000E ; bytes:1
gbl_tmr1h                        EQU	0x0000000F ; bytes:1
gbl_t1con                        EQU	0x00000010 ; bytes:1
gbl_tmr2                         EQU	0x00000011 ; bytes:1
gbl_t2con                        EQU	0x00000012 ; bytes:1
gbl_ccpr1l                       EQU	0x00000015 ; bytes:1
gbl_ccpr1h                       EQU	0x00000016 ; bytes:1
gbl_ccp1con                      EQU	0x00000017 ; bytes:1
gbl_rcsta                        EQU	0x00000018 ; bytes:1
gbl_txreg                        EQU	0x00000019 ; bytes:1
gbl_rcreg                        EQU	0x0000001A ; bytes:1
gbl_cmcon                        EQU	0x0000001F ; bytes:1
gbl_option_reg                   EQU	0x00000081 ; bytes:1
gbl_trisa                        EQU	0x00000085 ; bytes:1
gbl_trisb                        EQU	0x00000086 ; bytes:1
gbl_pie1                         EQU	0x0000008C ; bytes:1
gbl_pcon                         EQU	0x0000008E ; bytes:1
gbl_pr2                          EQU	0x00000092 ; bytes:1
gbl_txsta                        EQU	0x00000098 ; bytes:1
gbl_spbrg                        EQU	0x00000099 ; bytes:1
gbl_eedata                       EQU	0x0000009A ; bytes:1
gbl_eeadr                        EQU	0x0000009B ; bytes:1
gbl_eecon1                       EQU	0x0000009C ; bytes:1
gbl_eecon2                       EQU	0x0000009D ; bytes:1
gbl_vrcon                        EQU	0x0000009F ; bytes:1
gbl_input_saturator              EQU	0x0000002A ; bytes:4
gbl_input_state                  EQU	0x00000038 ; bytes:1
gbl_inhibit_input_state_updates  EQU	0x00000039 ; bit:0
gbl_second_counter               EQU	0x00000034 ; bytes:2
gbl_state_machines               EQU	0x0000002E ; bytes:3
gbl_second_timers                EQU	0x00000020 ; bytes:10
gbl_evaluate_cycle               EQU	0x00000039 ; bit:1
gbl_evaluate_quarter             EQU	0x0000003A ; bytes:1
gbl_OneWire_ConsecutiveRxFails   EQU	0x0000003B ; bytes:1
gbl_Param_OutputPinMap           EQU	0x0000003C ; bytes:1
gbl_Param_StartDelay             EQU	0x0000003D ; bytes:1
gbl_Param_RunOutTime             EQU	0x0000003E ; bytes:1
gbl_Param_MinOffTime             EQU	0x0000003F ; bytes:1
gbl_DBGLED_Heat                  EQU	0x00000005 ; bit:1
gbl_RELAY_ForceHVACOff           EQU	0x00000005 ; bit:2
gbl_DBGLED_AC_N                  EQU	0x00000006 ; bit:6
gbl_DBGLED_SuctionLn             EQU	0x00000005 ; bit:3
gbl_RELAY_ForceDampersOpen       EQU	0x00000006 ; bit:4
gbl_FreezeDetection_Sense        EQU	0x00000040 ; bytes:1
gbl_OverHeatDetection_Sense      EQU	0x00000041 ; bytes:1
gbl_FreezePrevention_State       EQU	0x00000042 ; bytes:1
gbl_OverHeatPrevention_State     EQU	0x00000043 ; bytes:1
gbl_OneWire_Accum_CRC            EQU	0x00000044 ; bytes:1
gbl_AnyNonZeroDataCap            EQU	0x00000039 ; bit:2
gbl_crc_rom                      EQU	0x00000045 ; bytes:1
gbl_TempData                     EQU	0x00000036 ; bytes:2
DoStateMac_00013_arg_sm_num      EQU	0x00000048 ; bytes:1
DoStateMac_00013_arg_enabl_00014 EQU	0x00000049 ; bit:0
DoStateMac_00013_1_current_state EQU	0x0000004A ; bytes:1
DoStateMac_00013_1_next_state    EQU	0x0000004B ; bytes:1
DoStateMac_00013_1_output__00015 EQU	0x0000004C ; bytes:1
CompTempVar553                   EQU	0x00000051 ; bytes:1
CompTempVar554                   EQU	0x00000052 ; bytes:1
CompTempVar555                   EQU	0x00000051 ; bytes:1
CompTempVar558                   EQU	0x00000051 ; bytes:1
CompTempVar559                   EQU	0x00000052 ; bytes:1
CompTempVar562                   EQU	0x00000051 ; bytes:1
CompTempVar563                   EQU	0x00000052 ; bytes:1
DoStateMac_00013_26_param_value  EQU	0x00000051 ; bytes:1
DoStateMac_00013_32_param_value  EQU	0x00000051 ; bytes:1
DoStateMac_00013_38_param_value  EQU	0x00000051 ; bytes:1
CompTempVar567                   EQU	0x00000051 ; bytes:1
CompTempVar569                   EQU	0x00000051 ; bytes:1
CompTempVar570                   EQU	0x00000052 ; bytes:1
FreezePrev_00016_1_current_state EQU	0x00000048 ; bytes:1
FreezePrev_00016_1_next_state    EQU	0x00000049 ; bytes:1
CompTempVar578                   EQU	0x0000004A ; bytes:1
OverHeatPr_00017_1_current_state EQU	0x00000048 ; bytes:1
OverHeatPr_00017_1_next_state    EQU	0x00000049 ; bytes:1
CompTempVar587                   EQU	0x0000004A ; bytes:1
CompTempVar589                   EQU	0x0000004B ; bytes:1
CompTempVarRet597                EQU	0x0000004B ; bytes:1
CompTempVar593                   EQU	0x00000048 ; bytes:1
CompTempVar594                   EQU	0x00000049 ; bytes:1
OneWire_Re_0001C_1_retData       EQU	0x0000004A ; bytes:1
OneWire_Re_0001C_2_i             EQU	0x0000004B ; bytes:1
CompTempVarRet599                EQU	0x0000004C ; bit:1
OneWire_Re_0001F_1_readData      EQU	0x0000004C ; bit:0
OneWire_Wr_00020_arg_data        EQU	0x0000004A ; bit:0
OneWire_Wr_0001B_arg_byteData    EQU	0x00000048 ; bytes:1
OneWire_Wr_0001B_2_i             EQU	0x00000049 ; bytes:1
CompTempVar595                   EQU	0x0000004B ; bytes:1
interrupt_5_port_byte            EQU	0x0000005E ; bytes:1
interrupt_6_i                    EQU	0x0000005F ; bytes:1
interrupt_18_i                   EQU	0x00000060 ; bytes:1
CompTempVar608                   EQU	0x00000061 ; bytes:1
CompTempVar609                   EQU	0x00000062 ; bytes:1
CompTempVar612                   EQU	0x00000062 ; bytes:1
CompTempVar614                   EQU	0x00000063 ; bytes:1
CompTempVar617                   EQU	0x00000060 ; bytes:1
CompTempVar618                   EQU	0x00000061 ; bytes:1
CompTempVar619                   EQU	0x00000062 ; bytes:1
CompTempVar620                   EQU	0x00000063 ; bytes:1
CompTempVar627                   EQU	0x00000062 ; bytes:1
CompTempVar628                   EQU	0x00000062 ; bytes:1
CompTempVar629                   EQU	0x00000063 ; bytes:1
CompTempVar632                   EQU	0x00000064 ; bytes:1
CompTempVar633                   EQU	0x00000065 ; bytes:1
CompTempVar636                   EQU	0x00000064 ; bytes:1
set_timer_00000_arg_tmr_num      EQU	0x00000056 ; bytes:1
set_timer_00000_arg_value        EQU	0x00000057 ; bytes:2
CompTempVar575                   EQU	0x00000059 ; bytes:1
CompTempVar576                   EQU	0x0000005A ; bytes:1
init_varia_00021_2_i             EQU	0x00000049 ; bytes:1
init_varia_00021_3_i             EQU	0x0000004A ; bytes:1
init_varia_00021_4_i             EQU	0x0000004B ; bytes:1
CompTempVar641                   EQU	0x0000004B ; bytes:1
CompTempVar642                   EQU	0x0000004C ; bytes:1
main_1_flash_byte                EQU	0x00000046 ; bytes:1
main_1_latched_evaluate_quarter  EQU	0x00000047 ; bytes:1
main_2_i                         EQU	0x00000048 ; bytes:1
CompTempVar644                   EQU	0x00000049 ; bit:0
CompTempVar645                   EQU	0x0000004A ; bytes:1
CompTempVar646                   EQU	0x0000004B ; bytes:1
CompTempVar647                   EQU	0x0000004C ; bytes:1
CompTempVar648                   EQU	0x0000004D ; bytes:1
CompTempVar649                   EQU	0x0000004E ; bytes:1
CompTempVar650                   EQU	0x0000004F ; bytes:1
CompTempVar651                   EQU	0x00000050 ; bytes:1
CompTempVar652                   EQU	0x00000051 ; bytes:1
CompTempVar653                   EQU	0x00000052 ; bytes:1
CompTempVar654                   EQU	0x00000053 ; bytes:1
CompTempVar655                   EQU	0x00000054 ; bytes:1
CompTempVar656                   EQU	0x0000004A ; bytes:1
CompTempVar657                   EQU	0x0000004B ; bytes:1
CompTempVar658                   EQU	0x0000004C ; bytes:1
CompTempVar659                   EQU	0x0000004D ; bytes:1
CompTempVar660                   EQU	0x0000004E ; bytes:1
CompTempVar661                   EQU	0x0000004F ; bytes:1
CompTempVar662                   EQU	0x0000004A ; bytes:1
CompTempVar663                   EQU	0x0000004B ; bytes:1
CompTempVar664                   EQU	0x0000004C ; bytes:1
CompTempVar665                   EQU	0x0000004D ; bytes:1
CompTempVar666                   EQU	0x0000004E ; bytes:1
CompTempVar667                   EQU	0x0000004F ; bytes:1
CompTempVar668                   EQU	0x00000048 ; bit:0
CompTempVar669                   EQU	0x00000048 ; bit:0
CompTempVar671                   EQU	0x00000049 ; bytes:1
CompTempVar672                   EQU	0x0000004A ; bytes:1
CompTempVar673                   EQU	0x0000004B ; bytes:1
CompTempVar674                   EQU	0x0000004C ; bytes:1
CompTempVar675                   EQU	0x0000004D ; bytes:1
CompTempVar676                   EQU	0x0000004E ; bytes:1
CompTempVar677                   EQU	0x00000051 ; bytes:1
CompTempVar679                   EQU	0x00000048 ; bytes:1
CompTempVar680                   EQU	0x00000049 ; bytes:1
CompTempVar681                   EQU	0x0000004A ; bytes:1
CompTempVar682                   EQU	0x00000048 ; bit:0
CompTempVar703                   EQU	0x00000048 ; bit:0
CompTempVar704                   EQU	0x00000049 ; bytes:1
CompTempVar705                   EQU	0x00000048 ; bit:0
delay_us_00000_arg_del           EQU	0x0000004D ; bytes:1
delay_ms_00000_arg_del           EQU	0x00000049 ; bytes:1
__rom_get_00000_arg_objNumb      EQU	0x0000004F ; bytes:1
__rom_get_00000_arg_idx          EQU	0x00000050 ; bytes:1
__rom_get_00000_1_romAddr        EQU	0x00000052 ; bytes:2
Int1Context                      EQU	0x0000007F ; bytes:1
Int1BContext                     EQU	0x00000031 ; bytes:3
	ORG 0x00000000
	GOTO	_startup
	ORG 0x00000004
	MOVWF Int1Context
	SWAPF STATUS, W
	BCF STATUS, RP0
	BCF STATUS, RP1
	MOVWF Int1BContext
	SWAPF PCLATH, W
	MOVWF Int1BContext+D'1'
	SWAPF FSR, W
	MOVWF Int1BContext+D'2'
	BCF PCLATH,3
	BCF PCLATH,4
	GOTO	interrupt
	ORG 0x00000010
delay_us_00000
; { delay_us ; function begin
	MOVLW 0x03
	ADDWF delay_us_00000_arg_del, F
	RRF delay_us_00000_arg_del, F
	RRF delay_us_00000_arg_del, F
	MOVLW 0x7F
	ANDWF delay_us_00000_arg_del, F
label1
	NOP
	DECFSZ delay_us_00000_arg_del, F
	GOTO	label1
	RETURN
; } delay_us function end

	ORG 0x0000001A
delay_ms_00000
; { delay_ms ; function begin
	MOVF delay_ms_00000_arg_del, F
	BTFSS STATUS,Z
	GOTO	label2
	RETURN
label2
	MOVLW 0xF9
label3
	ADDLW 0xFF
	BTFSS STATUS,Z
	GOTO	label3
	NOP
	DECFSZ delay_ms_00000_arg_del, F
	GOTO	label2
	RETURN
; } delay_ms function end

	ORG 0x00000026
__rom_get_00000
; { __rom_get ; function begin
	MOVF __rom_get_00000_arg_objNumb, W
	MOVWF __rom_get_00000_1_romAddr+D'1'
	CLRF __rom_get_00000_1_romAddr
	BCF STATUS,C
	RLF __rom_get_00000_1_romAddr+D'1', F
	RLF __rom_get_00000_1_romAddr, F
	RLF __rom_get_00000_1_romAddr+D'1', F
	RLF __rom_get_00000_1_romAddr, F
	ADDWF __rom_get_00000_1_romAddr+D'1', F
	BTFSC STATUS,C
	INCF __rom_get_00000_1_romAddr, F
	ADDWF __rom_get_00000_1_romAddr+D'1', F
	BTFSC STATUS,C
	INCF __rom_get_00000_1_romAddr, F
	MOVLW	LOW( label4 )
	ADDWF __rom_get_00000_1_romAddr+D'1', F
	BTFSC STATUS,C
	INCF __rom_get_00000_1_romAddr, F
	MOVLW	HIGH( label4 )
	ADDWF __rom_get_00000_1_romAddr, W
	MOVWF PCLATH
	MOVF __rom_get_00000_arg_idx, W
	MOVWF __rom_get_00000_1_romAddr
	MOVF __rom_get_00000_1_romAddr+D'1', W
	MOVWF PCL
label4
	MOVLW	HIGH( label5 )
	MOVWF PCLATH
	MOVLW	HIGH( label6 )
	MOVWF __rom_get_00000_1_romAddr+D'1'
	MOVLW	LOW( label6 )
	GOTO	label5
	MOVLW	HIGH( label5 )
	MOVWF PCLATH
	MOVLW	HIGH( label7 )
	MOVWF __rom_get_00000_1_romAddr+D'1'
	MOVLW	LOW( label7 )
	GOTO	label5
	MOVLW	HIGH( label5 )
	MOVWF PCLATH
	MOVLW	HIGH( label8 )
	MOVWF __rom_get_00000_1_romAddr+D'1'
	MOVLW	LOW( label8 )
	GOTO	label5
	MOVLW	HIGH( label5 )
	MOVWF PCLATH
	MOVLW	HIGH( label9 )
	MOVWF __rom_get_00000_1_romAddr+D'1'
	MOVLW	LOW( label9 )
	GOTO	label5
	MOVLW	HIGH( label10 )
	MOVWF __rom_get_00000_1_romAddr+D'1'
	MOVLW	LOW( label10 )
label5
	ADDWF __rom_get_00000_1_romAddr, F
	BTFSC STATUS,C
	INCF __rom_get_00000_1_romAddr+D'1', F
	MOVF __rom_get_00000_1_romAddr+D'1', W
	MOVWF PCLATH
	MOVF __rom_get_00000_1_romAddr, W
	MOVWF PCL
label6
	RETLW 0x00
	RETLW 0x07
	RETLW 0x06
label7
	RETLW 0x1E
	RETLW 0x02
	RETLW 0x02
label8
	RETLW 0x00
	RETLW 0x02
	RETLW 0x02
label9
	RETLW 0x3C
	RETLW 0x06
	RETLW 0x06
label10
	RETLW 0x00
	RETLW 0x5E
	RETLW 0xBC
	RETLW 0xE2
	RETLW 0x61
	RETLW 0x3F
	RETLW 0xDD
	RETLW 0x83
	RETLW 0xC2
	RETLW 0x9C
	RETLW 0x7E
	RETLW 0x20
	RETLW 0xA3
	RETLW 0xFD
	RETLW 0x1F
	RETLW 0x41
	RETLW 0x9D
	RETLW 0xC3
	RETLW 0x21
	RETLW 0x7F
	RETLW 0xFC
	RETLW 0xA2
	RETLW 0x40
	RETLW 0x1E
	RETLW 0x5F
	RETLW 0x01
	RETLW 0xE3
	RETLW 0xBD
	RETLW 0x3E
	RETLW 0x60
	RETLW 0x82
	RETLW 0xDC
	RETLW 0x23
	RETLW 0x7D
	RETLW 0x9F
	RETLW 0xC1
	RETLW 0x42
	RETLW 0x1C
	RETLW 0xFE
	RETLW 0xA0
	RETLW 0xE1
	RETLW 0xBF
	RETLW 0x5D
	RETLW 0x03
	RETLW 0x80
	RETLW 0xDE
	RETLW 0x3C
	RETLW 0x62
	RETLW 0xBE
	RETLW 0xE0
	RETLW 0x02
	RETLW 0x5C
	RETLW 0xDF
	RETLW 0x81
	RETLW 0x63
	RETLW 0x3D
	RETLW 0x7C
	RETLW 0x22
	RETLW 0xC0
	RETLW 0x9E
	RETLW 0x1D
	RETLW 0x43
	RETLW 0xA1
	RETLW 0xFF
	RETLW 0x46
	RETLW 0x18
	RETLW 0xFA
	RETLW 0xA4
	RETLW 0x27
	RETLW 0x79
	RETLW 0x9B
	RETLW 0xC5
	RETLW 0x84
	RETLW 0xDA
	RETLW 0x38
	RETLW 0x66
	RETLW 0xE5
	RETLW 0xBB
	RETLW 0x59
	RETLW 0x07
	RETLW 0xDB
	RETLW 0x85
	RETLW 0x67
	RETLW 0x39
	RETLW 0xBA
	RETLW 0xE4
	RETLW 0x06
	RETLW 0x58
	RETLW 0x19
	RETLW 0x47
	RETLW 0xA5
	RETLW 0xFB
	RETLW 0x78
	RETLW 0x26
	RETLW 0xC4
	RETLW 0x9A
	RETLW 0x65
	RETLW 0x3B
	RETLW 0xD9
	RETLW 0x87
	RETLW 0x04
	RETLW 0x5A
	RETLW 0xB8
	RETLW 0xE6
	RETLW 0xA7
	RETLW 0xF9
	RETLW 0x1B
	RETLW 0x45
	RETLW 0xC6
	RETLW 0x98
	RETLW 0x7A
	RETLW 0x24
	RETLW 0xF8
	RETLW 0xA6
	RETLW 0x44
	RETLW 0x1A
	RETLW 0x99
	RETLW 0xC7
	RETLW 0x25
	RETLW 0x7B
	RETLW 0x3A
	RETLW 0x64
	RETLW 0x86
	RETLW 0xD8
	RETLW 0x5B
	RETLW 0x05
	RETLW 0xE7
	RETLW 0xB9
	RETLW 0x8C
	RETLW 0xD2
	RETLW 0x30
	RETLW 0x6E
	RETLW 0xED
	RETLW 0xB3
	RETLW 0x51
	RETLW 0x0F
	RETLW 0x4E
	RETLW 0x10
	RETLW 0xF2
	RETLW 0xAC
	RETLW 0x2F
	RETLW 0x71
	RETLW 0x93
	RETLW 0xCD
	RETLW 0x11
	RETLW 0x4F
	RETLW 0xAD
	RETLW 0xF3
	RETLW 0x70
	RETLW 0x2E
	RETLW 0xCC
	RETLW 0x92
	RETLW 0xD3
	RETLW 0x8D
	RETLW 0x6F
	RETLW 0x31
	RETLW 0xB2
	RETLW 0xEC
	RETLW 0x0E
	RETLW 0x50
	RETLW 0xAF
	RETLW 0xF1
	RETLW 0x13
	RETLW 0x4D
	RETLW 0xCE
	RETLW 0x90
	RETLW 0x72
	RETLW 0x2C
	RETLW 0x6D
	RETLW 0x33
	RETLW 0xD1
	RETLW 0x8F
	RETLW 0x0C
	RETLW 0x52
	RETLW 0xB0
	RETLW 0xEE
	RETLW 0x32
	RETLW 0x6C
	RETLW 0x8E
	RETLW 0xD0
	RETLW 0x53
	RETLW 0x0D
	RETLW 0xEF
	RETLW 0xB1
	RETLW 0xF0
	RETLW 0xAE
	RETLW 0x4C
	RETLW 0x12
	RETLW 0x91
	RETLW 0xCF
	RETLW 0x2D
	RETLW 0x73
	RETLW 0xCA
	RETLW 0x94
	RETLW 0x76
	RETLW 0x28
	RETLW 0xAB
	RETLW 0xF5
	RETLW 0x17
	RETLW 0x49
	RETLW 0x08
	RETLW 0x56
	RETLW 0xB4
	RETLW 0xEA
	RETLW 0x69
	RETLW 0x37
	RETLW 0xD5
	RETLW 0x8B
	RETLW 0x57
	RETLW 0x09
	RETLW 0xEB
	RETLW 0xB5
	RETLW 0x36
	RETLW 0x68
	RETLW 0x8A
	RETLW 0xD4
	RETLW 0x95
	RETLW 0xCB
	RETLW 0x29
	RETLW 0x77
	RETLW 0xF4
	RETLW 0xAA
	RETLW 0x48
	RETLW 0x16
	RETLW 0xE9
	RETLW 0xB7
	RETLW 0x55
	RETLW 0x0B
	RETLW 0x88
	RETLW 0xD6
	RETLW 0x34
	RETLW 0x6A
	RETLW 0x2B
	RETLW 0x75
	RETLW 0x97
	RETLW 0xC9
	RETLW 0x4A
	RETLW 0x14
	RETLW 0xF6
	RETLW 0xA8
	RETLW 0x74
	RETLW 0x2A
	RETLW 0xC8
	RETLW 0x96
	RETLW 0x15
	RETLW 0x4B
	RETLW 0xA9
	RETLW 0xF7
	RETLW 0xB6
	RETLW 0xE8
	RETLW 0x0A
	RETLW 0x54
	RETLW 0xD7
	RETLW 0x89
	RETLW 0x6B
	RETLW 0x35
; } __rom_get function end

	ORG 0x0000016D
OneWire_Wr_00020
; { OneWire_Write ; function begin
	BCF gbl_intcon,7
	BTFSS OneWire_Wr_00020_arg_data,0
	GOTO	label11
	BCF gbl_portb,5
	BSF STATUS, RP0
	BCF gbl_trisb,5
	NOP
	NOP
	NOP
	NOP
	NOP
	BSF gbl_trisb,5
	GOTO	label12
label11
	BCF gbl_portb,5
	BSF STATUS, RP0
	BCF gbl_trisb,5
	MOVLW 0x41
	BCF STATUS, RP0
	MOVWF delay_us_00000_arg_del
	CALL delay_us_00000
	BSF STATUS, RP0
	BSF gbl_trisb,5
label12
	BSF gbl_intcon,7
	RETURN
; } OneWire_Write function end

	ORG 0x00000185
OneWire_Re_0001F
; { OneWire_ReadSlot ; function begin
	BCF STATUS, RP0
	BCF STATUS, RP1
	BCF OneWire_Re_0001F_1_readData,0
	BCF gbl_intcon,7
	BCF gbl_portb,5
	BSF STATUS, RP0
	BCF gbl_trisb,5
	NOP
	NOP
	NOP
	BSF gbl_trisb,5
	NOP
	NOP
	NOP
	NOP
	NOP
	NOP
	NOP
	NOP
	NOP
	BCF STATUS, RP0
	BTFSC gbl_portb,5
	BSF OneWire_Re_0001F_1_readData,0
	BSF gbl_intcon,7
	MOVLW 0x3C
	MOVWF delay_us_00000_arg_del
	CALL delay_us_00000
	BCF CompTempVarRet599,1
	BTFSC OneWire_Re_0001F_1_readData,0
	BSF CompTempVarRet599,1
	RETURN
; } OneWire_ReadSlot function end

	ORG 0x000001A4
set_timer_00000
; { set_timer ; function begin
	BCF gbl_intcon,7
	BCF	STATUS,IRP
	MOVLW LOW(gbl_second_timers+D'0')
	MOVWF FSR
	MOVF set_timer_00000_arg_tmr_num, W
	MOVWF CompTempVar575
	BCF STATUS,C
	RLF CompTempVar575, W
	ADDWF FSR, F
	INCF FSR, F
	MOVF set_timer_00000_arg_value, W
	MOVWF CompTempVar576
	MOVF set_timer_00000_arg_value+D'1', W
	MOVWF INDF
	DECF FSR, F
	MOVF CompTempVar576, W
	MOVWF INDF
	BSF gbl_intcon,7
	RETURN
; } set_timer function end

	ORG 0x000001B7
__mul_16u__0000C
; { __mul_16u_16u__16 ; function begin
	CLRF __mul_16u__0000C_1_i
	CLRF CompTempVarRet378
	CLRF CompTempVarRet378+D'1'
	MOVF __mul_16u__0000C_arg_a, W
	MOVWF __mul_16u__0000C_1_t
	MOVF __mul_16u__0000C_arg_a+D'1', W
	MOVWF __mul_16u__0000C_1_t+D'1'
label13
	BTFSC __mul_16u__0000C_1_i,4
	RETURN
	BTFSS __mul_16u__0000C_arg_b,0
	GOTO	label14
	MOVF __mul_16u__0000C_1_t, W
	ADDWF CompTempVarRet378, F
	MOVF __mul_16u__0000C_1_t+D'1', W
	BTFSC gbl_status,0
	INCFSZ __mul_16u__0000C_1_t+D'1', W
	ADDWF CompTempVarRet378+D'1', F
label14
	BCF gbl_status,0
	RRF __mul_16u__0000C_arg_b+D'1', F
	RRF __mul_16u__0000C_arg_b, F
	BCF gbl_status,0
	RLF __mul_16u__0000C_1_t, F
	RLF __mul_16u__0000C_1_t+D'1', F
	INCF __mul_16u__0000C_1_i, F
	GOTO	label13
; } __mul_16u_16u__16 function end

	ORG 0x000001D0
RxSuctionL_00018
; { RxSuctionLineTemp ; function begin
	BCF STATUS, RP0
	BCF STATUS, RP1
	CLRF gbl_OneWire_ConsecutiveRxFails
	BTFSS gbl_TempData+D'1',7
	GOTO	label15
	MOVLW 0x02
	MOVWF gbl_FreezeDetection_Sense
	GOTO	label19
label15
	MOVLW 0x2C
	SUBWF gbl_TempData, W
	MOVF gbl_TempData+D'1', W
	BTFSC STATUS,C
	GOTO	label16
	BTFSS STATUS,Z
	GOTO	label16
	MOVLW 0x02
	MOVWF gbl_FreezeDetection_Sense
	GOTO	label19
label16
	MOVLW 0x45
	SUBWF gbl_TempData, W
	MOVF gbl_TempData+D'1', W
	BTFSC STATUS,C
	GOTO	label17
	BTFSS STATUS,Z
	GOTO	label17
	MOVLW 0x01
	MOVWF gbl_FreezeDetection_Sense
	GOTO	label19
label17
	MOVF gbl_TempData, W
	SUBLW 0x74
	BTFSC STATUS,C
	MOVF gbl_TempData+D'1', W
	BTFSC STATUS,Z
	GOTO	label18
	MOVLW 0x04
	MOVWF gbl_FreezeDetection_Sense
	GOTO	label19
label18
	MOVLW 0x03
	MOVWF gbl_FreezeDetection_Sense
label19
	BTFSS gbl_TempData+D'1',7
	GOTO	label20
	MOVLW 0x04
	MOVWF gbl_OverHeatDetection_Sense
	RETURN
label20
	MOVF gbl_TempData+D'1', W
	SUBLW 0x04
	BTFSS STATUS,Z
	GOTO	label21
	MOVF gbl_TempData, W
	SUBLW 0x1A
label21
	BTFSC STATUS,C
	GOTO	label22
	MOVLW 0x01
	MOVWF gbl_OverHeatDetection_Sense
	RETURN
label22
	MOVLW 0x04
	MOVWF gbl_OverHeatDetection_Sense
	RETURN
; } RxSuctionLineTemp function end

	ORG 0x0000020A
OneWire_Wr_0001B
; { OneWire_WriteByte ; function begin
	CLRF OneWire_Wr_0001B_2_i
label23
	MOVLW 0x08
	SUBWF OneWire_Wr_0001B_2_i, W
	BTFSC STATUS,C
	RETURN
	MOVLW 0x01
	ANDWF OneWire_Wr_0001B_arg_byteData, W
	MOVWF CompTempVar595
	BCF OneWire_Wr_00020_arg_data,0
	MOVF CompTempVar595, W
	BTFSS STATUS,Z
	BSF OneWire_Wr_00020_arg_data,0
	CALL OneWire_Wr_00020
	NOP
	NOP
	NOP
	NOP
	NOP
	NOP
	NOP
	NOP
	BCF STATUS, RP0
	MOVF OneWire_Wr_0001B_arg_byteData, F
	BCF STATUS,C
	RRF OneWire_Wr_0001B_arg_byteData, F
	INCF OneWire_Wr_0001B_2_i, F
	GOTO	label23
; } OneWire_WriteByte function end

	ORG 0x00000225
OneWire_Se_0001A
; { OneWire_SendInit ; function begin
	BCF STATUS, RP0
	BCF STATUS, RP1
	BCF gbl_portb,5
	BSF STATUS, RP0
	BCF gbl_trisb,5
	MOVLW 0x01
	BCF STATUS, RP0
	MOVWF delay_ms_00000_arg_del
	CALL delay_ms_00000
	BSF STATUS, RP0
	BSF gbl_trisb,5
	RETURN
; } OneWire_SendInit function end

	ORG 0x00000231
OneWire_Re_0001C
; { OneWire_ReadByte ; function begin
	BCF STATUS, RP0
	BCF STATUS, RP1
	CLRF OneWire_Re_0001C_1_retData
	CLRF OneWire_Re_0001C_2_i
label24
	MOVLW 0x08
	SUBWF OneWire_Re_0001C_2_i, W
	BTFSC STATUS,C
	GOTO	label26
	MOVF OneWire_Re_0001C_1_retData, F
	BCF STATUS,C
	RRF OneWire_Re_0001C_1_retData, F
	CALL OneWire_Re_0001F
	BTFSS CompTempVarRet599,1
	GOTO	label25
	BSF OneWire_Re_0001C_1_retData,7
	BSF gbl_AnyNonZeroDataCap,2
label25
	INCF OneWire_Re_0001C_2_i, F
	GOTO	label24
label26
	MOVF gbl_crc_rom, W
	MOVWF __rom_get_00000_arg_objNumb
	MOVF OneWire_Re_0001C_1_retData, W
	XORWF gbl_OneWire_Accum_CRC, W
	MOVWF __rom_get_00000_arg_idx
	CALL __rom_get_00000
	MOVWF gbl_OneWire_Accum_CRC
	MOVF OneWire_Re_0001C_1_retData, W
	MOVWF CompTempVarRet597
	RETURN
; } OneWire_ReadByte function end

	ORG 0x0000024D
BadOrNoSuc_0001D
; { BadOrNoSuctionLineTempData ; function begin
	MOVLW 0x03
	BCF STATUS, RP0
	BCF STATUS, RP1
	SUBWF gbl_OneWire_ConsecutiveRxFails, W
	BTFSC STATUS,C
	GOTO	label27
	INCF gbl_OneWire_ConsecutiveRxFails, F
	RETURN
label27
	CLRF gbl_FreezeDetection_Sense
	CLRF gbl_OverHeatDetection_Sense
	RETURN
; } BadOrNoSuctionLineTempData function end

	ORG 0x00000258
setup_00000
; { setup ; function begin
	MOVLW 0x4F
	BSF STATUS, RP0
	BCF STATUS, RP1
	MOVWF gbl_option_reg
	BSF gbl_pcon,3
	MOVLW 0x07
	BCF STATUS, RP0
	MOVWF gbl_cmcon
	BSF STATUS, RP0
	CLRF gbl_vrcon
	BCF STATUS, RP0
	CLRF gbl_ccp1con
	BCF gbl_rcsta,7
	CLRF gbl_porta
	MOVLW 0x20
	BSF STATUS, RP0
	MOVWF gbl_trisa
	BCF STATUS, RP0
	CLRF gbl_portb
	MOVLW 0x0F
	BSF STATUS, RP0
	MOVWF gbl_trisb
	BCF STATUS, RP0
	BCF gbl_pir1,0
	MOVLW 0x01
	BSF STATUS, RP0
	MOVWF gbl_pie1
	BSF gbl_intcon,6
	MOVLW 0x37
	BCF STATUS, RP0
	MOVWF gbl_tmr1l
	MOVLW 0xFF
	MOVWF gbl_tmr1h
	MOVLW 0x21
	MOVWF gbl_t1con
	RETURN
; } setup function end

	ORG 0x0000027C
init_varia_00021
; { init_variables ; function begin
	BCF STATUS, RP0
	BCF STATUS, RP1
	CLRF init_varia_00021_4_i
label28
	MOVLW 0x04
	SUBWF init_varia_00021_4_i, W
	BTFSC STATUS,C
	GOTO	label29
	BCF	STATUS,IRP
	MOVLW LOW(gbl_input_saturator+D'0')
	MOVWF FSR
	MOVF init_varia_00021_4_i, W
	ADDWF FSR, F
	MOVLW 0x00
	MOVWF INDF
	INCF init_varia_00021_4_i, F
	GOTO	label28
label29
	CLRF init_varia_00021_3_i
label30
	MOVLW 0x05
	SUBWF init_varia_00021_3_i, W
	BTFSC STATUS,C
	GOTO	label31
	BCF	STATUS,IRP
	MOVLW LOW(gbl_second_timers+D'0')
	MOVWF FSR
	MOVF init_varia_00021_3_i, W
	MOVWF CompTempVar641
	BCF STATUS,C
	RLF CompTempVar641, W
	ADDWF FSR, F
	INCF FSR, F
	MOVLW 0x00
	MOVWF CompTempVar642
	CLRF INDF
	DECF FSR, F
	MOVF CompTempVar642, W
	MOVWF INDF
	INCF init_varia_00021_3_i, F
	GOTO	label30
label31
	CLRF init_varia_00021_2_i
label32
	MOVLW 0x03
	SUBWF init_varia_00021_2_i, W
	BTFSC STATUS,C
	GOTO	label33
	BCF	STATUS,IRP
	MOVLW LOW(gbl_state_machines+D'0')
	MOVWF FSR
	MOVF init_varia_00021_2_i, W
	ADDWF FSR, F
	MOVLW 0x00
	MOVWF INDF
	INCF init_varia_00021_2_i, F
	GOTO	label32
label33
	BCF gbl_evaluate_cycle,1
	CLRF gbl_evaluate_quarter
	CLRF gbl_second_counter
	CLRF gbl_second_counter+D'1'
	CLRF gbl_input_state
	BCF gbl_inhibit_input_state_updates,0
	CLRF gbl_OneWire_ConsecutiveRxFails
	CLRF gbl_FreezeDetection_Sense
	CLRF gbl_OverHeatDetection_Sense
	CLRF gbl_FreezePrevention_State
	CLRF gbl_OverHeatPrevention_State
	RETURN
; } init_variables function end

	ORG 0x000002BC
OverHeatPr_00017
; { OverHeatPreventionStateMachine ; function begin
	BCF STATUS, RP0
	BCF STATUS, RP1
	MOVF gbl_OverHeatPrevention_State, W
	MOVWF OverHeatPr_00017_1_current_state
	MOVWF OverHeatPr_00017_1_next_state
	MOVF OverHeatPr_00017_1_current_state, W
	XORLW 0x00
	BTFSC STATUS,Z
	GOTO	label34
	XORLW 0x01
	BTFSC STATUS,Z
	GOTO	label35
	XORLW 0x03
	BTFSC STATUS,Z
	GOTO	label37
	XORLW 0x01
	BTFSC STATUS,Z
	GOTO	label40
	XORLW 0x07
	BTFSC STATUS,Z
	GOTO	label41
	GOTO	label43
label34
	BTFSS gbl_input_state,1
	GOTO	label44
	MOVLW 0x01
	MOVWF OverHeatPr_00017_1_next_state
	GOTO	label44
label35
	MOVLW 0x02
	ANDWF gbl_input_state, W
	MOVWF CompTempVar587
	MOVF CompTempVar587, F
	BTFSS STATUS,Z
	GOTO	label36
	CLRF OverHeatPr_00017_1_next_state
	GOTO	label44
label36
	DECF gbl_OverHeatDetection_Sense, W
	BTFSS STATUS,Z
	GOTO	label44
	MOVLW 0x02
	MOVWF OverHeatPr_00017_1_next_state
	GOTO	label44
label37
	MOVLW 0x02
	ANDWF gbl_input_state, W
	MOVWF CompTempVar589
	MOVF CompTempVar589, F
	BTFSS STATUS,Z
	GOTO	label38
	CLRF OverHeatPr_00017_1_next_state
	GOTO	label44
label38
	DECF gbl_OverHeatDetection_Sense, W
	BTFSC STATUS,Z
	GOTO	label39
	MOVLW 0x01
	MOVWF OverHeatPr_00017_1_next_state
	GOTO	label44
label39
	MOVF gbl_second_timers+D'8', F
	BTFSS STATUS,Z
	GOTO	label44
	MOVF gbl_second_timers+D'9', F
	BTFSS STATUS,Z
	GOTO	label44
	MOVLW 0x03
	MOVWF OverHeatPr_00017_1_next_state
	GOTO	label44
label40
	MOVF gbl_second_timers+D'8', F
	BTFSS STATUS,Z
	GOTO	label44
	MOVF gbl_second_timers+D'9', F
	BTFSS STATUS,Z
	GOTO	label44
	MOVLW 0x04
	MOVWF OverHeatPr_00017_1_next_state
	GOTO	label44
label41
	DECF gbl_OverHeatDetection_Sense, W
	BTFSS STATUS,Z
	GOTO	label42
	MOVLW 0x02
	MOVWF OverHeatPr_00017_1_next_state
	GOTO	label44
label42
	MOVF gbl_second_timers+D'8', F
	BTFSS STATUS,Z
	GOTO	label44
	MOVF gbl_second_timers+D'9', F
	BTFSS STATUS,Z
	GOTO	label44
	CLRF OverHeatPr_00017_1_next_state
	GOTO	label44
label43
	CLRF OverHeatPr_00017_1_next_state
label44
	MOVF OverHeatPr_00017_1_current_state, W
	XORLW 0x02
	BTFSC STATUS,Z
	GOTO	label45
	MOVF OverHeatPr_00017_1_next_state, W
	XORLW 0x02
	BTFSS STATUS,Z
	GOTO	label45
	MOVLW 0x04
	MOVWF set_timer_00000_arg_tmr_num
	MOVLW 0x19
	MOVWF set_timer_00000_arg_value
	CLRF set_timer_00000_arg_value+D'1'
	CALL set_timer_00000
label45
	MOVF OverHeatPr_00017_1_current_state, W
	XORLW 0x03
	BTFSC STATUS,Z
	GOTO	label46
	MOVF OverHeatPr_00017_1_next_state, W
	XORLW 0x03
	BTFSS STATUS,Z
	GOTO	label46
	MOVLW 0x04
	MOVWF set_timer_00000_arg_tmr_num
	MOVLW 0x84
	MOVWF set_timer_00000_arg_value
	MOVLW 0x03
	MOVWF set_timer_00000_arg_value+D'1'
	CALL set_timer_00000
label46
	MOVF OverHeatPr_00017_1_current_state, W
	XORLW 0x04
	BTFSC STATUS,Z
	GOTO	label47
	MOVF OverHeatPr_00017_1_next_state, W
	XORLW 0x04
	BTFSS STATUS,Z
	GOTO	label47
	MOVLW 0x04
	MOVWF set_timer_00000_arg_tmr_num
	MOVLW 0x10
	MOVWF set_timer_00000_arg_value
	MOVLW 0x0E
	MOVWF set_timer_00000_arg_value+D'1'
	CALL set_timer_00000
label47
	MOVF OverHeatPr_00017_1_next_state, W
	MOVWF gbl_OverHeatPrevention_State
	RETURN
; } OverHeatPreventionStateMachine function end

	ORG 0x00000343
OneWire_St_0001E
; { OneWire_StartConversion ; function begin
	CALL OneWire_Se_0001A
	MOVLW 0x01
	BCF STATUS, RP0
	MOVWF delay_ms_00000_arg_del
	CALL delay_ms_00000
	MOVLW 0xCC
	MOVWF OneWire_Wr_0001B_arg_byteData
	CALL OneWire_Wr_0001B
	MOVLW 0x44
	MOVWF OneWire_Wr_0001B_arg_byteData
	CALL OneWire_Wr_0001B
	RETURN
; } OneWire_StartConversion function end

	ORG 0x0000034F
OneWire_Re_00019
; { OneWire_ReadConversion ; function begin
	CALL OneWire_Se_0001A
	MOVLW 0x01
	BCF STATUS, RP0
	MOVWF delay_ms_00000_arg_del
	CALL delay_ms_00000
	MOVLW 0xCC
	MOVWF OneWire_Wr_0001B_arg_byteData
	CALL OneWire_Wr_0001B
	MOVLW 0xBE
	MOVWF OneWire_Wr_0001B_arg_byteData
	CALL OneWire_Wr_0001B
	CLRF gbl_OneWire_Accum_CRC
	BCF gbl_AnyNonZeroDataCap,2
	CALL OneWire_Re_0001C
	MOVF CompTempVarRet597, W
	MOVWF gbl_TempData
	CLRF gbl_TempData+D'1'
	BCF PCLATH,3
	BCF PCLATH,4
	CALL OneWire_Re_0001C
	CLRF CompTempVar593
	MOVF CompTempVarRet597, W
	MOVWF CompTempVar594
	MOVF CompTempVar593, W
	IORWF gbl_TempData, F
	MOVF CompTempVar594, W
	IORWF gbl_TempData+D'1', F
	BCF PCLATH,3
	BCF PCLATH,4
	CALL OneWire_Re_0001C
	BCF PCLATH,3
	BCF PCLATH,4
	CALL OneWire_Re_0001C
	BCF PCLATH,3
	BCF PCLATH,4
	CALL OneWire_Re_0001C
	BCF PCLATH,3
	BCF PCLATH,4
	CALL OneWire_Re_0001C
	BCF PCLATH,3
	BCF PCLATH,4
	CALL OneWire_Re_0001C
	BCF PCLATH,3
	BCF PCLATH,4
	CALL OneWire_Re_0001C
	BCF PCLATH,3
	BCF PCLATH,4
	CALL OneWire_Re_0001C
	MOVF gbl_OneWire_Accum_CRC, F
	BCF PCLATH,3
	BCF PCLATH,4
	BTFSS STATUS,Z
	GOTO	label48
	BTFSS gbl_AnyNonZeroDataCap,2
	GOTO	label48
	CALL RxSuctionL_00018
	RETURN
label48
	CALL BadOrNoSuc_0001D
	RETURN
; } OneWire_ReadConversion function end

	ORG 0x0000038A
FreezePrev_00016
; { FreezePreventionStateMachine ; function begin
	BCF STATUS, RP0
	BCF STATUS, RP1
	MOVF gbl_FreezePrevention_State, W
	MOVWF FreezePrev_00016_1_current_state
	MOVWF FreezePrev_00016_1_next_state
	MOVLW 0x01
	ANDWF gbl_input_state, W
	MOVWF CompTempVar578
	MOVF CompTempVar578, F
	BTFSS STATUS,Z
	GOTO	label49
	MOVF FreezePrev_00016_1_current_state, W
	XORLW 0x06
	BTFSS STATUS,Z
	GOTO	label50
label49
	MOVF gbl_FreezeDetection_Sense, F
	BTFSS STATUS,Z
	GOTO	label51
label50
	CLRF FreezePrev_00016_1_next_state
	GOTO	label66
label51
	MOVF FreezePrev_00016_1_current_state, W
	XORLW 0x00
	BTFSC STATUS,Z
	GOTO	label52
	XORLW 0x01
	BTFSC STATUS,Z
	GOTO	label53
	XORLW 0x03
	BTFSC STATUS,Z
	GOTO	label54
	XORLW 0x01
	BTFSC STATUS,Z
	GOTO	label56
	XORLW 0x07
	BTFSC STATUS,Z
	GOTO	label58
	XORLW 0x01
	BTFSC STATUS,Z
	GOTO	label60
	XORLW 0x03
	BTFSC STATUS,Z
	GOTO	label62
	XORLW 0x01
	BTFSC STATUS,Z
	GOTO	label63
	GOTO	label65
label52
	BTFSS gbl_input_state,0
	GOTO	label66
	MOVLW 0x01
	MOVWF FreezePrev_00016_1_next_state
	GOTO	label66
label53
	MOVF gbl_second_timers+D'6', F
	BTFSS STATUS,Z
	GOTO	label66
	MOVF gbl_second_timers+D'7', F
	BTFSS STATUS,Z
	GOTO	label66
	MOVLW 0x02
	MOVWF FreezePrev_00016_1_next_state
	GOTO	label66
label54
	DECF gbl_FreezeDetection_Sense, W
	BTFSC STATUS,Z
	GOTO	label55
	MOVF gbl_FreezeDetection_Sense, W
	XORLW 0x02
	BTFSS STATUS,Z
	GOTO	label66
label55
	MOVLW 0x03
	MOVWF FreezePrev_00016_1_next_state
	GOTO	label66
label56
	DECF gbl_FreezeDetection_Sense, W
	BTFSC STATUS,Z
	GOTO	label57
	MOVF gbl_FreezeDetection_Sense, W
	XORLW 0x02
	BTFSC STATUS,Z
	GOTO	label57
	MOVLW 0x02
	MOVWF FreezePrev_00016_1_next_state
label57
	MOVF gbl_second_timers+D'6', F
	BTFSS STATUS,Z
	GOTO	label66
	MOVF gbl_second_timers+D'7', F
	BTFSS STATUS,Z
	GOTO	label66
	MOVLW 0x04
	MOVWF FreezePrev_00016_1_next_state
	GOTO	label66
label58
	MOVF gbl_FreezeDetection_Sense, W
	XORLW 0x02
	BTFSS STATUS,Z
	GOTO	label59
	MOVF gbl_second_timers+D'6', F
	BTFSS STATUS,Z
	GOTO	label59
	MOVF gbl_second_timers+D'7', F
	BTFSS STATUS,Z
	GOTO	label59
	MOVLW 0x05
	MOVWF FreezePrev_00016_1_next_state
label59
	MOVF gbl_FreezeDetection_Sense, W
	XORLW 0x04
	BTFSS STATUS,Z
	GOTO	label66
	MOVLW 0x07
	MOVWF FreezePrev_00016_1_next_state
	GOTO	label66
label60
	MOVF gbl_second_timers+D'6', F
	BTFSS STATUS,Z
	GOTO	label61
	MOVF gbl_second_timers+D'7', F
	BTFSS STATUS,Z
	GOTO	label61
	MOVLW 0x06
	MOVWF FreezePrev_00016_1_next_state
label61
	MOVF gbl_FreezeDetection_Sense, W
	XORLW 0x02
	BTFSC STATUS,Z
	GOTO	label66
	MOVLW 0x04
	MOVWF FreezePrev_00016_1_next_state
	GOTO	label66
label62
	MOVF gbl_second_timers+D'6', F
	BTFSS STATUS,Z
	GOTO	label66
	MOVF gbl_second_timers+D'7', F
	BTFSS STATUS,Z
	GOTO	label66
	MOVLW 0x02
	MOVWF FreezePrev_00016_1_next_state
	GOTO	label66
label63
	MOVF gbl_second_timers+D'6', F
	BTFSS STATUS,Z
	GOTO	label64
	MOVF gbl_second_timers+D'7', F
	BTFSS STATUS,Z
	GOTO	label64
	MOVLW 0x02
	MOVWF FreezePrev_00016_1_next_state
label64
	MOVF gbl_FreezeDetection_Sense, W
	XORLW 0x04
	BTFSC STATUS,Z
	GOTO	label66
	MOVLW 0x04
	MOVWF FreezePrev_00016_1_next_state
	GOTO	label66
label65
	CLRF FreezePrev_00016_1_next_state
label66
	DECF FreezePrev_00016_1_current_state, W
	BTFSC STATUS,Z
	GOTO	label67
	DECF FreezePrev_00016_1_next_state, W
	BTFSS STATUS,Z
	GOTO	label67
	MOVLW 0x03
	MOVWF set_timer_00000_arg_tmr_num
	MOVLW 0xB4
	MOVWF set_timer_00000_arg_value
	CLRF set_timer_00000_arg_value+D'1'
	CALL set_timer_00000
label67
	MOVF FreezePrev_00016_1_current_state, W
	XORLW 0x03
	BTFSC STATUS,Z
	GOTO	label68
	MOVF FreezePrev_00016_1_next_state, W
	XORLW 0x03
	BTFSS STATUS,Z
	GOTO	label68
	MOVLW 0x03
	MOVWF set_timer_00000_arg_tmr_num
	MOVLW 0x14
	MOVWF set_timer_00000_arg_value
	CLRF set_timer_00000_arg_value+D'1'
	CALL set_timer_00000
label68
	MOVF FreezePrev_00016_1_current_state, W
	XORLW 0x03
	BTFSS STATUS,Z
	GOTO	label69
	MOVF FreezePrev_00016_1_next_state, W
	XORLW 0x04
	BTFSS STATUS,Z
	GOTO	label69
	MOVLW 0x03
	MOVWF set_timer_00000_arg_tmr_num
	MOVLW 0x50
	MOVWF set_timer_00000_arg_value
	CLRF set_timer_00000_arg_value+D'1'
	CALL set_timer_00000
label69
	MOVF FreezePrev_00016_1_current_state, W
	XORLW 0x05
	BTFSC STATUS,Z
	GOTO	label70
	MOVF FreezePrev_00016_1_next_state, W
	XORLW 0x05
	BTFSS STATUS,Z
	GOTO	label70
	MOVLW 0x03
	MOVWF set_timer_00000_arg_tmr_num
	MOVLW 0x14
	MOVWF set_timer_00000_arg_value
	CLRF set_timer_00000_arg_value+D'1'
	CALL set_timer_00000
label70
	MOVF FreezePrev_00016_1_current_state, W
	XORLW 0x06
	BTFSC STATUS,Z
	GOTO	label71
	MOVF FreezePrev_00016_1_next_state, W
	XORLW 0x06
	BTFSS STATUS,Z
	GOTO	label71
	MOVLW 0x03
	MOVWF set_timer_00000_arg_tmr_num
	MOVLW 0x08
	MOVWF set_timer_00000_arg_value
	MOVLW 0x07
	MOVWF set_timer_00000_arg_value+D'1'
	CALL set_timer_00000
label71
	MOVF FreezePrev_00016_1_current_state, W
	XORLW 0x07
	BTFSC STATUS,Z
	GOTO	label72
	MOVF FreezePrev_00016_1_next_state, W
	XORLW 0x07
	BTFSS STATUS,Z
	GOTO	label72
	MOVLW 0x03
	MOVWF set_timer_00000_arg_tmr_num
	MOVLW 0x2C
	MOVWF set_timer_00000_arg_value
	MOVLW 0x01
	MOVWF set_timer_00000_arg_value+D'1'
	CALL set_timer_00000
label72
	MOVF FreezePrev_00016_1_next_state, W
	MOVWF gbl_FreezePrevention_State
	RETURN
; } FreezePreventionStateMachine function end

	ORG 0x00000474
DoStateMac_00013
; { DoStateMachine ; function begin
	BCF	STATUS,IRP
	MOVLW LOW(gbl_state_machines+D'0')
	MOVWF FSR
	MOVF DoStateMac_00013_arg_sm_num, W
	ADDWF FSR, F
	MOVF INDF, W
	MOVWF DoStateMac_00013_1_current_state
	MOVWF DoStateMac_00013_1_next_state
	MOVF DoStateMac_00013_1_current_state, W
	XORLW 0x00
	BTFSC STATUS,Z
	GOTO	label73
	XORLW 0x01
	BTFSC STATUS,Z
	GOTO	label74
	XORLW 0x03
	BTFSC STATUS,Z
	GOTO	label76
	XORLW 0x01
	BTFSC STATUS,Z
	GOTO	label78
	XORLW 0x07
	BTFSC STATUS,Z
	GOTO	label79
	GOTO	label80
label73
	BTFSS DoStateMac_00013_arg_enabl_00014,0
	GOTO	label81
	MOVLW 0x01
	MOVWF DoStateMac_00013_1_next_state
	GOTO	label81
label74
	BTFSC DoStateMac_00013_arg_enabl_00014,0
	GOTO	label75
	CLRF DoStateMac_00013_1_next_state
	GOTO	label81
label75
	BCF	STATUS,IRP
	MOVLW LOW(gbl_second_timers+D'0')
	MOVWF FSR
	MOVF DoStateMac_00013_arg_sm_num, W
	MOVWF CompTempVar553
	BCF STATUS,C
	RLF CompTempVar553, W
	ADDWF FSR, F
	MOVF INDF, W
	MOVWF CompTempVar554
	INCF FSR, F
	MOVF CompTempVar554, F
	BTFSS STATUS,Z
	GOTO	label81
	MOVF INDF, F
	BTFSS STATUS,Z
	GOTO	label81
	MOVLW 0x02
	MOVWF DoStateMac_00013_1_next_state
	GOTO	label81
label76
	BTFSC DoStateMac_00013_arg_enabl_00014,0
	GOTO	label81
	MOVF gbl_Param_RunOutTime, W
	MOVWF __rom_get_00000_arg_objNumb
	MOVF DoStateMac_00013_arg_sm_num, W
	MOVWF __rom_get_00000_arg_idx
	CALL __rom_get_00000
	MOVWF CompTempVar555
	MOVF CompTempVar555, F
	BCF PCLATH,3
	BCF PCLATH,4
	BTFSS STATUS,Z
	GOTO	label77
	MOVLW 0x04
	MOVWF DoStateMac_00013_1_next_state
	GOTO	label81
label77
	MOVLW 0x03
	MOVWF DoStateMac_00013_1_next_state
	GOTO	label81
label78
	BCF	STATUS,IRP
	MOVLW LOW(gbl_second_timers+D'0')
	MOVWF FSR
	MOVF DoStateMac_00013_arg_sm_num, W
	MOVWF CompTempVar558
	BCF STATUS,C
	RLF CompTempVar558, W
	ADDWF FSR, F
	MOVF INDF, W
	MOVWF CompTempVar559
	INCF FSR, F
	MOVF CompTempVar559, F
	BTFSS STATUS,Z
	GOTO	label81
	MOVF INDF, F
	BTFSS STATUS,Z
	GOTO	label81
	MOVLW 0x04
	MOVWF DoStateMac_00013_1_next_state
	GOTO	label81
label79
	BCF	STATUS,IRP
	MOVLW LOW(gbl_second_timers+D'0')
	MOVWF FSR
	MOVF DoStateMac_00013_arg_sm_num, W
	MOVWF CompTempVar562
	BCF STATUS,C
	RLF CompTempVar562, W
	ADDWF FSR, F
	MOVF INDF, W
	MOVWF CompTempVar563
	INCF FSR, F
	MOVF CompTempVar563, F
	BTFSS STATUS,Z
	GOTO	label81
	MOVF INDF, F
	BTFSS STATUS,Z
	GOTO	label81
	CLRF DoStateMac_00013_1_next_state
	GOTO	label81
label80
	CLRF DoStateMac_00013_1_next_state
label81
	DECF DoStateMac_00013_1_current_state, W
	BTFSC STATUS,Z
	GOTO	label82
	DECF DoStateMac_00013_1_next_state, W
	BTFSS STATUS,Z
	GOTO	label82
	MOVF gbl_Param_StartDelay, W
	MOVWF __rom_get_00000_arg_objNumb
	MOVF DoStateMac_00013_arg_sm_num, W
	MOVWF __rom_get_00000_arg_idx
	CALL __rom_get_00000
	MOVWF DoStateMac_00013_26_param_value
	MOVF DoStateMac_00013_arg_sm_num, W
	MOVWF set_timer_00000_arg_tmr_num
	MOVF DoStateMac_00013_26_param_value, W
	MOVWF __mul_16u__0000C_arg_a
	CLRF __mul_16u__0000C_arg_a+D'1'
	MOVLW 0x0A
	MOVWF __mul_16u__0000C_arg_b
	CLRF __mul_16u__0000C_arg_b+D'1'
	BCF PCLATH,3
	BCF PCLATH,4
	CALL __mul_16u__0000C
	MOVF CompTempVarRet378, W
	MOVWF set_timer_00000_arg_value
	MOVF CompTempVarRet378+D'1', W
	MOVWF set_timer_00000_arg_value+D'1'
	CALL set_timer_00000
label82
	MOVF DoStateMac_00013_1_current_state, W
	XORLW 0x03
	BTFSC STATUS,Z
	GOTO	label83
	MOVF DoStateMac_00013_1_next_state, W
	XORLW 0x03
	BTFSS STATUS,Z
	GOTO	label83
	MOVF gbl_Param_RunOutTime, W
	MOVWF __rom_get_00000_arg_objNumb
	MOVF DoStateMac_00013_arg_sm_num, W
	MOVWF __rom_get_00000_arg_idx
	CALL __rom_get_00000
	MOVWF DoStateMac_00013_32_param_value
	MOVF DoStateMac_00013_arg_sm_num, W
	MOVWF set_timer_00000_arg_tmr_num
	MOVF DoStateMac_00013_32_param_value, W
	MOVWF __mul_16u__0000C_arg_a
	CLRF __mul_16u__0000C_arg_a+D'1'
	MOVLW 0x0A
	MOVWF __mul_16u__0000C_arg_b
	CLRF __mul_16u__0000C_arg_b+D'1'
	BCF PCLATH,3
	BCF PCLATH,4
	CALL __mul_16u__0000C
	MOVF CompTempVarRet378, W
	MOVWF set_timer_00000_arg_value
	MOVF CompTempVarRet378+D'1', W
	MOVWF set_timer_00000_arg_value+D'1'
	CALL set_timer_00000
label83
	MOVF DoStateMac_00013_1_current_state, W
	XORLW 0x04
	BTFSC STATUS,Z
	GOTO	label84
	MOVF DoStateMac_00013_1_next_state, W
	XORLW 0x04
	BTFSS STATUS,Z
	GOTO	label84
	MOVF gbl_Param_MinOffTime, W
	MOVWF __rom_get_00000_arg_objNumb
	MOVF DoStateMac_00013_arg_sm_num, W
	MOVWF __rom_get_00000_arg_idx
	CALL __rom_get_00000
	MOVWF DoStateMac_00013_38_param_value
	MOVF DoStateMac_00013_arg_sm_num, W
	MOVWF set_timer_00000_arg_tmr_num
	MOVF DoStateMac_00013_38_param_value, W
	MOVWF __mul_16u__0000C_arg_a
	CLRF __mul_16u__0000C_arg_a+D'1'
	MOVLW 0x0A
	MOVWF __mul_16u__0000C_arg_b
	CLRF __mul_16u__0000C_arg_b+D'1'
	BCF PCLATH,3
	BCF PCLATH,4
	CALL __mul_16u__0000C
	MOVF CompTempVarRet378, W
	MOVWF set_timer_00000_arg_value
	MOVF CompTempVarRet378+D'1', W
	MOVWF set_timer_00000_arg_value+D'1'
	CALL set_timer_00000
label84
	MOVF gbl_Param_OutputPinMap, W
	MOVWF __rom_get_00000_arg_objNumb
	MOVF DoStateMac_00013_arg_sm_num, W
	MOVWF __rom_get_00000_arg_idx
	CALL __rom_get_00000
	MOVWF DoStateMac_00013_1_output__00015
	MOVF DoStateMac_00013_1_next_state, W
	XORLW 0x02
	BCF PCLATH,3
	BCF PCLATH,4
	BTFSC STATUS,Z
	GOTO	label85
	MOVF DoStateMac_00013_1_next_state, W
	XORLW 0x03
	BTFSC STATUS,Z
	GOTO	label85
	BTFSS gbl_RELAY_ForceDampersOpen,4
	GOTO	label88
	MOVF DoStateMac_00013_arg_sm_num, W
	XORLW 0x02
	BTFSC STATUS,Z
	GOTO	label85
	DECF DoStateMac_00013_arg_sm_num, W
	BTFSS STATUS,Z
	GOTO	label88
label85
	MOVLW 0x01
	MOVWF CompTempVar567
	MOVF DoStateMac_00013_1_output__00015, W
label86
	ANDLW 0xFF
	BTFSC STATUS,Z
	GOTO	label87
	BCF STATUS,C
	RLF CompTempVar567, F
	ADDLW 0xFF
	GOTO	label86
label87
	MOVF CompTempVar567, W
	IORWF gbl_porta, F
	GOTO	label91
label88
	MOVLW 0x01
	MOVWF CompTempVar569
	CLRF CompTempVar570
	MOVF DoStateMac_00013_1_output__00015, W
label89
	ANDLW 0xFF
	BTFSC STATUS,Z
	GOTO	label90
	BCF STATUS,C
	RLF CompTempVar569, F
	RLF CompTempVar570, F
	ADDLW 0xFF
	GOTO	label89
label90
	COMF CompTempVar569, W
	ANDWF gbl_porta, F
label91
	BCF	STATUS,IRP
	MOVLW LOW(gbl_state_machines+D'0')
	MOVWF FSR
	MOVF DoStateMac_00013_arg_sm_num, W
	ADDWF FSR, F
	MOVF DoStateMac_00013_1_next_state, W
	MOVWF INDF
	RETURN
; } DoStateMachine function end

	ORG 0x00000579
main
; { main ; function begin
	BCF STATUS, RP0
	BCF STATUS, RP1
	CLRF main_1_flash_byte
	CLRF main_1_latched_evaluate_quarter
	CALL setup_00000
	CALL init_varia_00021
	BSF gbl_intcon,7
	CLRF main_2_i
label92
	MOVLW 0x04
	SUBWF main_2_i, W
	BTFSC STATUS,C
	GOTO	label95
	BTFSS main_2_i,0
	GOTO	label93
	BSF CompTempVar644,0
	GOTO	label94
label93
	BCF CompTempVar644,0
label94
	BTFSC CompTempVar644,0
	BSF gbl_DBGLED_AC_N,6
	BTFSS CompTempVar644,0
	BCF gbl_DBGLED_AC_N,6
	MOVLW 0x64
	MOVWF delay_ms_00000_arg_del
	CALL delay_ms_00000
	INCF main_2_i, F
	GOTO	label92
label95
	BTFSS gbl_evaluate_cycle,1
	GOTO	label95
	MOVF gbl_evaluate_quarter, W
	MOVWF main_1_latched_evaluate_quarter
	BCF gbl_evaluate_cycle,1
	MOVF main_1_latched_evaluate_quarter, F
	BTFSC STATUS,Z
	GOTO	label113
	MOVF main_1_latched_evaluate_quarter, W
	XORLW 0x06
	BTFSC STATUS,Z
	GOTO	label113
	BSF gbl_inhibit_input_state_updates,0
	CLRF DoStateMac_00013_arg_sm_num
	MOVLW 0x02
	ANDWF gbl_input_state, W
	MOVWF CompTempVar648
	MOVLW 0x08
	ANDWF gbl_input_state, W
	MOVWF CompTempVar646
	CLRF CompTempVar647
	MOVF CompTempVar646, F
	BTFSC STATUS,Z
	INCF CompTempVar647, F
	CLRF CompTempVar651
	MOVF CompTempVar647, F
	BTFSS STATUS,Z
	MOVF CompTempVar648, F
	BTFSS STATUS,Z
	INCF CompTempVar651, F
	MOVLW 0x04
	ANDWF gbl_input_state, W
	MOVWF CompTempVar649
	CLRF CompTempVar650
	MOVF CompTempVar649, F
	BTFSC STATUS,Z
	INCF CompTempVar650, F
	CLRF CompTempVar653
	MOVF CompTempVar650, F
	BTFSS STATUS,Z
	MOVF CompTempVar651, F
	BTFSS STATUS,Z
	INCF CompTempVar653, F
	CLRF CompTempVar652
	MOVF gbl_OverHeatPrevention_State, W
	SUBLW 0x03
	BTFSS STATUS,Z
	INCF CompTempVar652, F
	CLRF CompTempVar655
	MOVF CompTempVar652, F
	BTFSS STATUS,Z
	MOVF CompTempVar653, F
	BTFSS STATUS,Z
	INCF CompTempVar655, F
	CLRF CompTempVar654
	MOVF gbl_OverHeatPrevention_State, W
	SUBLW 0x04
	BTFSS STATUS,Z
	INCF CompTempVar654, F
	CLRF CompTempVar645
	MOVF CompTempVar654, F
	BTFSS STATUS,Z
	MOVF CompTempVar655, F
	BTFSS STATUS,Z
	INCF CompTempVar645, F
	BCF DoStateMac_00013_arg_enabl_00014,0
	MOVF CompTempVar645, W
	BTFSS STATUS,Z
	BSF DoStateMac_00013_arg_enabl_00014,0
	CALL DoStateMac_00013
	MOVLW 0x02
	MOVWF DoStateMac_00013_arg_sm_num
	MOVLW 0x02
	ANDWF gbl_input_state, W
	MOVWF CompTempVar661
	MOVLW 0x01
	ANDWF gbl_input_state, W
	MOVWF CompTempVar660
	CLRF CompTempVar659
	MOVF CompTempVar660, F
	BTFSC STATUS,Z
	MOVF CompTempVar661, F
	BTFSS STATUS,Z
	INCF CompTempVar659, F
	MOVLW 0x08
	ANDWF gbl_input_state, W
	MOVWF CompTempVar657
	CLRF CompTempVar658
	MOVF CompTempVar657, F
	BTFSC STATUS,Z
	INCF CompTempVar658, F
	CLRF CompTempVar656
	MOVF CompTempVar658, F
	BTFSS STATUS,Z
	MOVF CompTempVar659, F
	BTFSS STATUS,Z
	INCF CompTempVar656, F
	BCF DoStateMac_00013_arg_enabl_00014,0
	MOVF CompTempVar656, W
	BTFSS STATUS,Z
	BSF DoStateMac_00013_arg_enabl_00014,0
	CALL DoStateMac_00013
	MOVLW 0x01
	MOVWF DoStateMac_00013_arg_sm_num
	MOVLW 0x02
	ANDWF gbl_input_state, W
	MOVWF CompTempVar667
	MOVLW 0x01
	ANDWF gbl_input_state, W
	MOVWF CompTempVar666
	CLRF CompTempVar665
	MOVF CompTempVar666, F
	BTFSC STATUS,Z
	MOVF CompTempVar667, F
	BTFSS STATUS,Z
	INCF CompTempVar665, F
	MOVLW 0x04
	ANDWF gbl_input_state, W
	MOVWF CompTempVar663
	CLRF CompTempVar664
	MOVF CompTempVar663, F
	BTFSC STATUS,Z
	INCF CompTempVar664, F
	CLRF CompTempVar662
	MOVF CompTempVar664, F
	BTFSS STATUS,Z
	MOVF CompTempVar665, F
	BTFSS STATUS,Z
	INCF CompTempVar662, F
	BCF DoStateMac_00013_arg_enabl_00014,0
	MOVF CompTempVar662, W
	BTFSS STATUS,Z
	BSF DoStateMac_00013_arg_enabl_00014,0
	CALL DoStateMac_00013
	BTFSS gbl_input_state,0
	GOTO	label96
	BCF CompTempVar668,0
	GOTO	label97
label96
	BSF CompTempVar668,0
label97
	BTFSC CompTempVar668,0
	BSF gbl_DBGLED_AC_N,6
	BTFSS CompTempVar668,0
	BCF gbl_DBGLED_AC_N,6
	BTFSS gbl_input_state,1
	GOTO	label100
	MOVF gbl_Param_OutputPinMap, W
	MOVWF __rom_get_00000_arg_objNumb
	MOVLW 0x00
	CLRF __rom_get_00000_arg_idx
	CALL __rom_get_00000
	MOVWF CompTempVar677
	MOVLW 0x01
	MOVWF CompTempVar675
	CLRF CompTempVar676
	MOVF CompTempVar677, W
label98
	ANDLW 0xFF
	BCF PCLATH,3
	BCF PCLATH,4
	BTFSC STATUS,Z
	GOTO	label99
	BCF STATUS,C
	RLF CompTempVar675, F
	RLF CompTempVar676, F
	ADDLW 0xFF
	GOTO	label98
label99
	MOVF CompTempVar675, W
	ANDWF gbl_porta, W
	MOVWF CompTempVar673
	CLRF CompTempVar674
	MOVLW 0x01
	ANDWF main_1_flash_byte, W
	MOVWF CompTempVar672
	CLRF CompTempVar671
	MOVF CompTempVar673, F
	BTFSC STATUS,Z
	MOVF CompTempVar674, F
	BTFSS STATUS,Z
	MOVF CompTempVar672, F
	BTFSS STATUS,Z
	INCF CompTempVar671, F
	MOVF CompTempVar671, F
	BTFSS STATUS,Z
	GOTO	label100
	BSF CompTempVar669,0
	GOTO	label101
label100
	BCF CompTempVar669,0
label101
	BTFSC CompTempVar669,0
	BSF gbl_DBGLED_Heat,1
	BTFSS CompTempVar669,0
	BCF gbl_DBGLED_Heat,1
	CALL FreezePrev_00016
	CALL OverHeatPr_00017
	CLRF CompTempVar680
	MOVLW 0x06
	SUBWF gbl_FreezePrevention_State, W
	BTFSC STATUS,Z
	INCF CompTempVar680, F
	CLRF CompTempVar679
	MOVLW 0x03
	SUBWF gbl_OverHeatPrevention_State, W
	BTFSC STATUS,Z
	INCF CompTempVar679, F
	CLRF CompTempVar681
	MOVF CompTempVar679, F
	BTFSC STATUS,Z
	MOVF CompTempVar680, F
	BTFSS STATUS,Z
	INCF CompTempVar681, F
	BTFSC CompTempVar681,0
	BSF gbl_RELAY_ForceHVACOff,2
	BTFSS CompTempVar681,0
	BCF gbl_RELAY_ForceHVACOff,2
	MOVF gbl_FreezePrevention_State, W
	XORLW 0x04
	BTFSC STATUS,Z
	GOTO	label102
	MOVF gbl_FreezePrevention_State, W
	XORLW 0x05
	BTFSC STATUS,Z
	GOTO	label102
	MOVF gbl_FreezePrevention_State, W
	XORLW 0x07
	BTFSC STATUS,Z
	GOTO	label102
	MOVF gbl_FreezePrevention_State, W
	XORLW 0x06
	BTFSC STATUS,Z
	GOTO	label102
	MOVF gbl_OverHeatPrevention_State, W
	XORLW 0x03
	BTFSC STATUS,Z
	GOTO	label102
	BTFSS gbl_RELAY_ForceHVACOff,2
	GOTO	label103
label102
	BSF CompTempVar682,0
	GOTO	label104
label103
	BCF CompTempVar682,0
label104
	BTFSC CompTempVar682,0
	BSF gbl_RELAY_ForceDampersOpen,4
	BTFSS CompTempVar682,0
	BCF gbl_RELAY_ForceDampersOpen,4
	MOVF gbl_FreezeDetection_Sense, F
	BTFSS STATUS,Z
	GOTO	label107
	MOVLW 0x03
	ANDWF main_1_flash_byte, W
	MOVWF CompTempVar704
	MOVF CompTempVar704, F
	BTFSC STATUS,Z
	GOTO	label105
	BSF CompTempVar703,0
	GOTO	label106
label105
	BCF CompTempVar703,0
label106
	BTFSC CompTempVar703,0
	BSF gbl_DBGLED_SuctionLn,3
	BTFSS CompTempVar703,0
	BCF gbl_DBGLED_SuctionLn,3
	GOTO	label112
label107
	BTFSS gbl_RELAY_ForceHVACOff,2
	GOTO	label110
	BTFSS main_1_flash_byte,0
	GOTO	label108
	BSF CompTempVar705,0
	GOTO	label109
label108
	BCF CompTempVar705,0
label109
	BTFSC CompTempVar705,0
	BSF gbl_DBGLED_SuctionLn,3
	BTFSS CompTempVar705,0
	BCF gbl_DBGLED_SuctionLn,3
	GOTO	label112
label110
	BTFSS gbl_RELAY_ForceDampersOpen,4
	GOTO	label111
	BCF gbl_DBGLED_SuctionLn,3
	GOTO	label112
label111
	BCF gbl_DBGLED_SuctionLn,3
label112
	INCF main_1_flash_byte, F
	BCF gbl_inhibit_input_state_updates,0
label113
	MOVF main_1_latched_evaluate_quarter, F
	BTFSC STATUS,Z
	CALL OneWire_St_0001E
	MOVF main_1_latched_evaluate_quarter, W
	XORLW 0x06
	BTFSC STATUS,Z
	CALL OneWire_Re_00019
	CLRWDT
	GOTO	label95
; } main function end

	ORG 0x000006B1
_startup
	MOVLW 0x00
	BCF STATUS, RP0
	BCF STATUS, RP1
	MOVWF gbl_Param_OutputPinMap
	MOVLW 0x01
	MOVWF gbl_Param_StartDelay
	MOVLW 0x02
	MOVWF gbl_Param_RunOutTime
	MOVLW 0x03
	MOVWF gbl_Param_MinOffTime
	CLRF gbl_OneWire_Accum_CRC
	BCF gbl_AnyNonZeroDataCap,2
	MOVLW 0x04
	MOVWF gbl_crc_rom
	BCF PCLATH,3
	BCF PCLATH,4
	GOTO	main
	ORG 0x000006C2
interrupt
; { interrupt ; function begin
	BCF STATUS, RP0
	BCF STATUS, RP1
	BTFSS gbl_pir1,0
	GOTO	label137
	MOVLW 0x0B
	MOVWF gbl_tmr1l
	MOVLW 0xFF
	MOVWF gbl_tmr1h
	BCF gbl_pir1,0
	BTFSC gbl_evaluate_cycle,1
	GOTO	label116
	MOVLW 0xFA
	XORWF gbl_second_counter, W
	BTFSC STATUS,Z
	MOVF gbl_second_counter+D'1', W
	BTFSC STATUS,Z
	GOTO	label115
	MOVF gbl_second_counter, W
	XORLW 0xF4
	MOVLW 0x01
	BTFSC STATUS,Z
	XORWF gbl_second_counter+D'1', W
	BTFSC STATUS,Z
	GOTO	label115
	MOVF gbl_second_counter, W
	XORLW 0xEE
	MOVLW 0x02
	BTFSC STATUS,Z
	XORWF gbl_second_counter+D'1', W
	BTFSC STATUS,Z
	GOTO	label115
	MOVF gbl_second_counter, F
	BTFSS STATUS,Z
	GOTO	label116
	MOVF gbl_second_counter+D'1', F
	BTFSS STATUS,Z
	GOTO	label116
label115
	INCF gbl_evaluate_quarter, F
	MOVLW 0x08
	SUBWF gbl_evaluate_quarter, W
	BTFSC STATUS,C
	CLRF gbl_evaluate_quarter
	BSF gbl_evaluate_cycle,1
label116
	INCF gbl_second_counter, F
	BTFSC STATUS,Z
	INCF gbl_second_counter+D'1', F
	MOVLW 0x03
	SUBWF gbl_second_counter+D'1', W
	BTFSS STATUS,Z
	GOTO	label117
	MOVLW 0xE8
	SUBWF gbl_second_counter, W
label117
	BTFSS STATUS,C
	GOTO	label122
	CLRF interrupt_18_i
label118
	MOVLW 0x05
	SUBWF interrupt_18_i, W
	BTFSC STATUS,C
	GOTO	label121
	BCF	STATUS,IRP
	MOVLW LOW(gbl_second_timers+D'0')
	MOVWF FSR
	MOVF interrupt_18_i, W
	MOVWF CompTempVar608
	BCF STATUS,C
	RLF CompTempVar608, W
	ADDWF FSR, F
	MOVF INDF, W
	MOVWF CompTempVar609
	INCF FSR, F
	MOVF CompTempVar609, F
	BTFSS STATUS,Z
	GOTO	label119
	MOVF INDF, F
	BTFSC STATUS,Z
	GOTO	label120
label119
	BCF	STATUS,IRP
	MOVLW LOW(gbl_second_timers+D'0')
	MOVWF FSR
	MOVF interrupt_18_i, W
	MOVWF CompTempVar612
	BCF STATUS,C
	RLF CompTempVar612, F
	MOVF CompTempVar612, W
	ADDWF FSR, F
	MOVF INDF, W
	MOVWF CompTempVar614
	INCF FSR, F
	MOVF CompTempVar614, F
	BTFSC STATUS,Z
	DECF INDF, F
	DECF CompTempVar614, F
	DECF FSR, F
	MOVF CompTempVar614, W
	MOVWF INDF
label120
	INCF interrupt_18_i, F
	GOTO	label118
label121
	CLRF gbl_second_counter
	CLRF gbl_second_counter+D'1'
label122
	MOVF gbl_portb, W
	MOVWF interrupt_5_port_byte
	CLRF interrupt_6_i
label123
	MOVLW 0x04
	SUBWF interrupt_6_i, W
	BTFSC STATUS,C
	GOTO	label137
	MOVLW 0x01
	MOVWF CompTempVar619
	CLRF CompTempVar620
	MOVF interrupt_6_i, W
label124
	ANDLW 0xFF
	BTFSC STATUS,Z
	GOTO	label125
	BCF STATUS,C
	RLF CompTempVar619, F
	RLF CompTempVar620, F
	ADDLW 0xFF
	GOTO	label124
label125
	MOVF CompTempVar619, W
	ANDWF interrupt_5_port_byte, W
	MOVWF CompTempVar617
	CLRF CompTempVar618
	MOVF CompTempVar617, F
	BTFSS STATUS,Z
	GOTO	label127
	MOVF CompTempVar618, F
	BTFSS STATUS,Z
	GOTO	label127
	BCF	STATUS,IRP
	MOVLW LOW(gbl_input_saturator+D'0')
	MOVWF FSR
	MOVF interrupt_6_i, W
	ADDWF FSR, F
	MOVLW 0xE6
	SUBWF INDF, W
	BTFSC STATUS,C
	GOTO	label126
	BCF	STATUS,IRP
	MOVLW LOW(gbl_input_saturator+D'0')
	MOVWF FSR
	MOVF interrupt_6_i, W
	ADDWF FSR, F
	MOVLW 0x19
	ADDWF INDF, F
	GOTO	label128
label126
	BCF	STATUS,IRP
	MOVLW LOW(gbl_input_saturator+D'0')
	MOVWF FSR
	MOVF interrupt_6_i, W
	ADDWF FSR, F
	MOVLW 0xFF
	MOVWF INDF
	GOTO	label128
label127
	BCF	STATUS,IRP
	MOVLW LOW(gbl_input_saturator+D'0')
	MOVWF FSR
	MOVF interrupt_6_i, W
	ADDWF FSR, F
	MOVF INDF, W
	SUBLW 0x00
	BTFSC STATUS,C
	GOTO	label128
	BCF	STATUS,IRP
	MOVLW LOW(gbl_input_saturator+D'0')
	MOVWF FSR
	MOVF interrupt_6_i, W
	MOVWF CompTempVar627
	MOVF CompTempVar627, W
	ADDWF FSR, F
	DECF INDF, F
label128
	BTFSC gbl_inhibit_input_state_updates,0
	GOTO	label136
	MOVLW 0x01
	MOVWF CompTempVar628
	CLRF CompTempVar629
	MOVF interrupt_6_i, W
label129
	ANDLW 0xFF
	BTFSC STATUS,Z
	GOTO	label130
	BCF STATUS,C
	RLF CompTempVar628, F
	RLF CompTempVar629, F
	ADDLW 0xFF
	GOTO	label129
label130
	MOVF CompTempVar628, W
	ANDWF gbl_input_state, W
	BTFSC STATUS,Z
	GOTO	label133
	BCF	STATUS,IRP
	MOVLW LOW(gbl_input_saturator+D'0')
	MOVWF FSR
	MOVF interrupt_6_i, W
	ADDWF FSR, F
	MOVLW 0x14
	SUBWF INDF, W
	BTFSC STATUS,C
	GOTO	label136
	MOVLW 0x01
	MOVWF CompTempVar632
	CLRF CompTempVar633
	MOVF interrupt_6_i, W
label131
	ANDLW 0xFF
	BTFSC STATUS,Z
	GOTO	label132
	BCF STATUS,C
	RLF CompTempVar632, F
	RLF CompTempVar633, F
	ADDLW 0xFF
	GOTO	label131
label132
	COMF CompTempVar632, W
	ANDWF gbl_input_state, F
	GOTO	label136
label133
	BCF	STATUS,IRP
	MOVLW LOW(gbl_input_saturator+D'0')
	MOVWF FSR
	MOVF interrupt_6_i, W
	ADDWF FSR, F
	MOVF INDF, W
	SUBLW 0xC8
	BTFSC STATUS,C
	GOTO	label136
	MOVLW 0x01
	MOVWF CompTempVar636
	MOVF interrupt_6_i, W
label134
	ANDLW 0xFF
	BTFSC STATUS,Z
	GOTO	label135
	BCF STATUS,C
	RLF CompTempVar636, F
	ADDLW 0xFF
	GOTO	label134
label135
	MOVF CompTempVar636, W
	IORWF gbl_input_state, F
label136
	INCF interrupt_6_i, F
	GOTO	label123
label137
	SWAPF Int1BContext+D'2', W
	MOVWF FSR
	SWAPF Int1BContext+D'1', W
	MOVWF PCLATH
	SWAPF Int1BContext, W
	MOVWF STATUS
	SWAPF Int1Context, F
	SWAPF Int1Context, W
	RETFIE
; } interrupt function end

	ORG 0x00002007
	DW 0x0054
	END
