//
//  program.c
//
//
//  Created by Brian Lewis on 1/25/16.
//
//
// Target: 16F648A
//  Clock: 4MHz
//
// NOTE: I/Os max sink/source is 25mA!!!
//
//    ForceHVACOff        1 RA2     RA1 18  Heat (dbgled) 10mA to LED
// OneWire/FrzProtect     2 RA3     RA0 17  2ndHeat       10mA to SSR, <=10mA to LED
//                        3 RA4     RA7 16  Fans-US       10mA to SSR, <=15mA to LED
//                        4 MCLR    RA6 15  Fans-DS       10mA to SSR, <=15mA to LED
//                        5 vss     vdd 14
//               AC       6 RB0     PGD 13
//             HEAT       7 RB1     PGC 12  AC (dbgled)
//         DAMPERUP       8 RB2     RB5 11  1wire
//       DAMPERDOWN       9 RB3     PGM 10  ForceDampersOpen (RB4?)
//

// Watchdog Timer
//   - CONFIG[2] = 1      Enable WDT
//   - OPTION[3] = 1      PSA to WDT
//   - OPTION[2:0] = 0x7
//
// Indicator LEDs
//
//    AC On                   | Blue
//    Heat                    | Red
//      / 2ndHeat             | / Yellow Flashing
//    Both Dampers Open       | Green
//    Upstairs Duct Fan Run   | Green
//    Downstairs Duct Fan Run | Green

//    OneWire Fail            | Yellow, 3 seconds on, 1 second off
//      / Force Dampers Open  | Solid RED
//      / Force AC Off        | Flashing RED

#include <system.h>

#pragma CLOCK_FREQ  4000000

// Configuration word:
//  bit 6:  BOREN   (Brown Out Reset)
//  bit 4:  INTOSC
//  bit 2:  WDTEN
// 101 0100

#pragma DATA        0x2007, 0x0054

// Inputs RB0..RB3 (pins 6,7,8,9)
#define INPUT_AC          0
#define INPUT_HEAT        1
#define INPUT_DAMPER_UP   2
#define INPUT_DAMPER_DOWN 3

unsigned char   input_saturator[4];
unsigned char   input_state;                  // bitvector, [0]..[3] correspond to PORTB RB0..RB3
bool            inhibit_input_state_updates;  // set by main loop when state machine code is active to prevent input_state from updating inside ISR

unsigned short  second_counter;

// 3 separate state machines to manage 2nd Stage Heat enablement, Upstairs duct booster fan, and a
// potential (future) downstairs duct boost fan
#define STATE_MACHINE_2NDSTAGE_HEAT      0
#define STATE_MACHINE_DUCTFAN_UPSTAIRS   1
#define STATE_MACHINE_DUCTFAN_DOWNSTAIRS 2
#define NUM_STATE_MACHINES               3

#define SECOND_TIMER_FREEZE_PREVENTION   3
#define SECOND_TIMER_OVERHEAT_PREVENTION 4
#define NUM_SECOND_TIMERS                5

// Comments for state machine state use 2nd stage heat as an example, but the same states are used for duct fans as well.
//==================
enum fsm_staging_e {
//==================
    E_PEND_CONDITIONS,   // idle state, waiting for HEAT + both dampers open
    E_WAIT_STARTTIME,    // here we wait out the start delay.  if conditions go away, then back to PEND_CONDITIONS
    E_ACTIVE,            // conditions met, enable 2nd stage heat.  stay here until conditions go away
    E_RUNOUT_ACTIVE,     // continue enabling output for RunOutTime.
    E_WAIT_MINOFFTIME    // disable 2nd stage heat.  wait here for min off delay
};

unsigned char   state_machines  [NUM_STATE_MACHINES]; // uses fsm_staging_e enumerations
unsigned short  second_timers   [NUM_SECOND_TIMERS];

bool            evaluate_cycle;
unsigned char   evaluate_quarter;
unsigned char   OneWire_ConsecutiveRxFails;

// =======================================
// Parameters
// =======================================

rom char *Param_OutputPinMap = {  0,  // 2nd Stage Heat on PORTA.RA0
                                  7,  // UPSTAIRS Duct Fan on PORTA.RA7
                                  6   // DownSTAIRS Duct Fan on PORTA.RA6
};

// Delays are stored as # of seconds / 10

rom char *Param_StartDelay    = {  30,     // 2nd Stage Heat start delay.  5 minutes = 300 seconds, store as 30
                                   2,      // Upstairs Duct Fan start delay.  2 == 20 seconds
                                   2       // Downstairs Duct Fan start delay.  2 == 20 seconds
};
rom char *Param_RunOutTime    = {  0,      // 2nd Stage Heat runout ontime--turn off as soon as a damper closes.
                                   2,      // Upstairs Duct Fan time to stay on after conditions no longer met.  20 seconds.
                                   2       // Downstairs Duct Fan time to stay on.
};
rom char *Param_MinOffTime    = {  60,     // 2nd Stage Heat lockout time.  This will be on top of start delay.  10 minutes.  store as 60.
                                   6,      // Upstairs Duct Fan lockout time = 1 minute.
                                   6       // Downstairs Duct Fan lockout time.
};

volatile bit  DBGLED_Heat         @ PORTA.1;
volatile bit  RELAY_ForceHVACOff  @ PORTA.2;
volatile bit  DBGLED_AC_N         @ PORTB.6;


//    OneWire Fail            | Yellow (3 seconds on, 1 second off)
//      / Force Dampers Open  | Solid RED
//      / Force AC Off        | Flashing RED
//      / OneWire OK          | Solid OFF
//
// LED across RA4 and RB4
// RELAY_ForceDampersOpen DBGLED_SuctionLn |      State        |                   Color
// ---------------------- ----------------   -----------------                  ------------
//           0                  0              Good 1wire/no action                 off
//           0                  1              No 1wire Rx                          yellow
//           1                  0              Forcing Dampers / Forcing AC Off     red
//

volatile bit  DBGLED_SuctionLn        @ PORTA.3;
volatile bit  RELAY_ForceDampersOpen  @ PORTB.4;

#define ONEWIREPIN_DRIVE   { clear_bit(portb, 5); clear_bit(trisb, 5); }
#define ONEWIREPIN_RELEASE { set_bit  (trisb, 5); }
#define ONEWIREPIN_TEST      test_bit (portb, 5)

//==================
enum fd_status_e {
//==================
  E_FD_NODATA,
  E_FD_BAD1,      // Open Dampers
  E_FD_BAD2,      // Kill AC
  E_FD_DEADBAND,
  E_FD_GOOD
};

unsigned char FreezeDetection_Sense;    // fd_status_e
unsigned char OverHeatDetection_Sense;  // fd_status_e

//==================
enum fp_staging_e {
//==================
  E_FP_AC_OFF_IDLE,                         // [AC OFF] -> E_FP_AC_OFF_IDLE;  [AC ON]  -> E_FP_AC_ON_DETECTION_START_DELAY
  E_FP_AC_ON_DETECTION_START_DELAY,         // [AC OFF] -> E_FP_AC_OFF_IDLE;  [3 min timer elapsed] ->  E_FP_AC_ON_DETECTION_ACTIVE
  E_FP_AC_ON_DETECTION_ACTIVE,              // [AC OFF] -> E_FP_AC_OFF_IDLE;  [Sense == BAD]  -> E_FP_AC_ON_DETECT_BAD_ENFORCE_DELAY
  E_FP_AC_ON_DETECT_BAD_ENFORCE_DELAY,      // [AC OFF] -> E_FP_AC_OFF_IDLE;  [Sense != BAD]  -> E_FP_AC_ON_DETECTION_ACTIVE;  [20 sec timer elapsed] -> E_FP_AC_ON_DETECT_BAD_FORCE_DAMPERS_OPEN
  E_FP_AC_ON_DETECT_BAD_FORCE_DAMPERS_OPEN, // [AC OFF] -> E_FP_AC_OFF_IDLE;  [Sense == GOOD] -> E_FP_AC_ON_DETECT_GOOD_RELAX_DELAY;  [Sense == BAD2] -> E_FP_DETECT_BAD_FORCE_AC_OFF
  E_FP_AC_ON_DETECT_BAD_FORCE_DAMPERS_OPEN_WAIT_OFF,
  E_FP_DETECT_BAD_FORCE_AC_OFF,             // [30 min timer elapsed] -> E_FP_AC_ON_DETECTION_ACTIVE
  E_FP_AC_ON_DETECT_GOOD_RELAX_DELAY        // [AC OFF] -> E_FP_AC_OFF_IDLE;  [Sense != GOOD] -> E_FP_AC_ON_DETECT_BAD_FORCE_DAMPERS_OPEN; [5 min timer elapsed] -> E_FP_AC_ON_DETECTION_ACTIVE
};

unsigned char FreezePrevention_State; //  fp_staging_e

//==================
enum ohp_staging_e {
//==================
  E_OH_HEAT_OFF_IDLE,
  E_OH_HEAT_ON_DETECTION_ACTIVE,
  E_OH_HEAT_ON_DETECTION_PENDING_TRIP,
  E_OH_HEAT_TRIP_DWELL,
  E_OH_HEAT_POST_TRIP_2ND_STAGE_INHIBIT
};

unsigned char OverHeatPrevention_State; // ohp_staging_e

// --------------------------
// Function Prototypes

void DoStateMachine(unsigned char sm_num, bool enabling_conditions);
void FreezePreventionStateMachine();
void OverHeatPreventionStateMachine();
void RxSuctionLineTemp();
void OneWire_ReadConversion();
void OneWire_StartConversion();
unsigned char OneWire_ReadByte();
bool OneWire_ReadSlot();
void OneWire_SendInit();
void OneWire_Write(bool data);
void OneWire_WriteByte(unsigned char byteData);
void BadOrNoSuctionLineTempData();


// -------------------------------
void setup () {
// -------------------------------

  option_reg = 0x4f;  // 0100 1111
  set_bit(pcon, 3);   // 4MHz

  cmcon   = 0x7;  // disable comparators
  vrcon   = 0x0;
  ccp1con = 0x0;  // ccp1con to 1 per pdf???


  clear_bit(rcsta, SPEN); // spen to 0
  
  // Ports
  porta = 0;    // outputs inactive
  trisa = 0x20; // drive all outputs, except RA5 (MCLR)

  portb = 0;    // one-wire output is active-low, controlled by trisb.RB5
  trisb = 0xf;  // RB0..RB3 inputs

  clear_bit (pir1,    TMR1IF);
  pie1 = 0x01;                   // Only enabling TMR1IE
  set_bit   (intcon,  PEIE);

  // Timer1 Control
  // Timer will run at 1MHz rate. We want to have an int every 1mSec (1kHz, 1000 ticks), so if we
  // divide down by 4, then we can avoid TMR1H/TMR1L funkiness and just set TMR1L to 255-200, and TMR1H
  // to 255 (0xff).  There may be some additional error, but it's small and the timers don't need to
  // be super precise

  // [5:4]  T1CKPS   0x10 (1:4 prescale)
  // [3]    T1OSCEN  0
  // [1]    TMR1CS   0    (Fosc/4)
  // [0]    TMR1EN   1

  tmr1l=55;
  tmr1h=255;

  t1con = 0x21;

}



// ------------------------------------
void interrupt() {
// ------------------------------------

  if (test_bit(pir1,TMR1IF)) { // rate = 1mSec

    tmr1l=11; // empirically adjusted from 55 to 11.  probably ISR delays accouting for the difference.
    tmr1h=255;

    clear_bit(pir1, TMR1IF);

    // Timekeeping
    // -----------
    
    if (!evaluate_cycle) {
      if (  second_counter == 250
         || second_counter == 500
         || second_counter == 750
         || second_counter == 0
         ) {
        if (++evaluate_quarter>=8) {
          evaluate_quarter=0;
        }
        evaluate_cycle=1;
      }
    }

    if (++second_counter >= 1000) {
      for (char i=0;i<NUM_SECOND_TIMERS;++i) {
        if (second_timers[i] != 0) {
          second_timers[i]--;
        }
      }
      second_counter = 0;
    }

    // Input sampling
    // --------------
    unsigned char port_byte = portb; // snapshot

    // 16 samples per AC period.
    // Assume saturator is at 0, and AC is present, and we only detect 2 pulses ON per period.
    //   Per period, saturator goes up 50, and down (16-2 = 14).  After period X, saturator is:
    //     1     36
    //     2     72
    //     3     108
    //     4     144
    //     5     180
    //     6     216
    //     7     252
    // Assume saturator is 252, and AC goes off:
    //     0      252
    //     1      236
    //     2      220
    //     3      204
    //     4      188
    //     8      124
    //     12     60
    //     16     0
    for (char i=0;i<4;++i) {
      if (!(port_byte & (1<<i))) {
        if (input_saturator[i] < (255-25)) {
          input_saturator[i]+=25;
        } else {
          input_saturator[i]=0xff;
        }
      } else {
        if (input_saturator[i]>0) {
          input_saturator[i]--;
        }
      }
      if (!inhibit_input_state_updates) {
        if (input_state & (1<<i)) {
          // state indicator was set, clear it when saturator is below 20
          if (input_saturator[i] < 20) {
            clear_bit(input_state, i);
          }
        } else {
          // state indicator was clear, set it when saturator is above 200
          if (input_saturator[i] > 200) {
            set_bit(input_state, i);
          }
        }
      } // !inhibit_input_state_updates
    } // for input loop


  } // TMR1IF
}

// ------------------------------------
void set_timer(unsigned char tmr_num, unsigned short value) {
// ------------------------------------
  clear_bit(intcon, GIE);
  second_timers[tmr_num] = value;
// TESTING second_timers[tmr_num] = 1 + (value>>4);
  
  set_bit(intcon, GIE);
}

// ------------------------------------
void init_variables() {
// ------------------------------------

  for (char i=0;i<4;++i) {
    input_saturator[i]=0;
  }

  for (char i=0;i<NUM_SECOND_TIMERS;++i) {
    second_timers[i]=0;
  }

  for (char i=0;i<NUM_STATE_MACHINES;++i) {
    state_machines[i]=E_PEND_CONDITIONS;
  }

  evaluate_cycle=0;
  evaluate_quarter=0;
  second_counter=0;
  input_state=0;
  inhibit_input_state_updates=0;

  OneWire_ConsecutiveRxFails=0;

  FreezeDetection_Sense     = E_FD_NODATA;
  OverHeatDetection_Sense   = E_FD_NODATA;
  FreezePrevention_State    = E_FP_AC_OFF_IDLE;
  OverHeatPrevention_State  = E_OH_HEAT_OFF_IDLE;
}

// ------------------------------------
void main() {
// ------------------------------------

  unsigned char flash_byte=0;
  unsigned char latched_evaluate_quarter=0;

  setup();

  init_variables();

  set_bit(intcon, GIE);
  
  for (unsigned char i=0;i<4;++i) {
    DBGLED_AC_N = (i&1) ? 1 : 0;
    delay_ms(100);
  }

  while (1) {

    if (!evaluate_cycle) {
      // wait for timer interrupt to tell us it's been at least 1/4 second
      continue;
    }

    latched_evaluate_quarter = evaluate_quarter; // if evaluate_cycle is set, then it's safe to read evaluate_quarter
    evaluate_cycle = 0;
    
    // evaluate_quarter:
    //  0: clear_wdt, start OO conversion
    //  1: clear_wdt, do state machines
    //  2: clear_wdt, do state machines
    //  3: clear_wdt, do state machines
    //  4: clear_wdt, do state machines     (start convert + 1 sec)
    //  5: clear_wdt, do state machines
    //  6: clear_wdt, sample OO temp sensor (start convert + 1.5 sec)
    //  7: clear_wdt, do state machines

    if (  latched_evaluate_quarter != 0
       && latched_evaluate_quarter != 6
       ) {
      
      inhibit_input_state_updates = 1;
    
      DoStateMachine(STATE_MACHINE_2NDSTAGE_HEAT, (  test_bit(input_state, INPUT_HEAT)                  // Heat on
                                                  && !test_bit(input_state, INPUT_DAMPER_DOWN)          // Both Dampers Open
                                                  && !test_bit(input_state, INPUT_DAMPER_UP)            // Both Dampers Open
                                                  && (OverHeatPrevention_State != E_OH_HEAT_TRIP_DWELL) // No OverHeat Protection or 2nd stage Inhibit
                                                  && (OverHeatPrevention_State != E_OH_HEAT_POST_TRIP_2ND_STAGE_INHIBIT)
                                                  ));

      DoStateMachine(STATE_MACHINE_DUCTFAN_DOWNSTAIRS, (  (  test_bit(input_state, INPUT_HEAT)
                                                          || test_bit(input_state, INPUT_AC)
                                                          )
                                                       && !test_bit(input_state, INPUT_DAMPER_DOWN)
                                                       ));

      DoStateMachine(STATE_MACHINE_DUCTFAN_UPSTAIRS,   (  (  test_bit(input_state, INPUT_HEAT)
                                                          || test_bit(input_state, INPUT_AC)
                                                          )
                                                       && !test_bit(input_state, INPUT_DAMPER_UP)
                                                       ));

      // Debug LED updates
      DBGLED_AC_N       = (test_bit(input_state, INPUT_AC)) ? 0 : 1;

      // The HEAT/2ND HEAT LED is the same LED, which changes color based on current direction.
      // It is connected between pins 18 and 17 (RA1, RA0).  Drive the Heat pin high when we see
      // the HVAC in Heat mode, but drive it low if we're enabling 2ndStage Heat, so that the 2nd State
      // color will light.  Make it flash in 2nd stage for color clarity.

      DBGLED_Heat       = (  test_bit(input_state, INPUT_HEAT)
                          && !(  test_bit(porta, Param_OutputPinMap[STATE_MACHINE_2NDSTAGE_HEAT])
                              && (flash_byte & 1)
                              )
                          ) ? 1 : 0;
      
      FreezePreventionStateMachine();
      OverHeatPreventionStateMachine();
      
      RELAY_ForceHVACOff = (FreezePrevention_State == E_FP_DETECT_BAD_FORCE_AC_OFF)
                        || (OverHeatPrevention_State == E_OH_HEAT_TRIP_DWELL);

      // TODO if we add AC force-off, then be sure to also drive RELAY_ForceDampersOpen to 1 while in this state as well.
      RELAY_ForceDampersOpen = (  (FreezePrevention_State == E_FP_AC_ON_DETECT_BAD_FORCE_DAMPERS_OPEN)
                               || (FreezePrevention_State == E_FP_AC_ON_DETECT_BAD_FORCE_DAMPERS_OPEN_WAIT_OFF)
                               || (FreezePrevention_State == E_FP_AC_ON_DETECT_GOOD_RELAX_DELAY)
                               || (FreezePrevention_State == E_FP_DETECT_BAD_FORCE_AC_OFF)
                               || (OverHeatPrevention_State == E_OH_HEAT_TRIP_DWELL)
                               || RELAY_ForceHVACOff
                               ) ? 1 : 0;
      
      if (FreezeDetection_Sense == E_FD_NODATA) {
        DBGLED_SuctionLn = ((flash_byte & 3) != 0) ? 1 : 0; // RELAY_ForceDampersOpen = off if FreezeDetection_Sense == NODATA.  This turns LED to YELLOW
      } else {
        if (RELAY_ForceHVACOff) {
          DBGLED_SuctionLn = (flash_byte & 1) ? 1 : 0;      // This flashes LED to RED (because RELAY_ForceDampersOpen is supposed to be 1 if ForceAcOff)
        } else if (RELAY_ForceDampersOpen) {
          DBGLED_SuctionLn = 0;         // This turns LED to RED
        } else {
          DBGLED_SuctionLn = 0;         // This turns LED to OFF
        }
      }

      flash_byte++;

      inhibit_input_state_updates = 0;
    }

    if (latched_evaluate_quarter==0) {
      OneWire_StartConversion();
    }

    if (latched_evaluate_quarter==6) {
      OneWire_ReadConversion();
    }
    
    clear_wdt();
        
  }
}

// ------------------------------------
void DoStateMachine(unsigned char sm_num, bool enabling_conditions) {
// ------------------------------------
  unsigned char current_state = state_machines[sm_num];
  unsigned char next_state    = current_state;

  switch (current_state) {

    case E_PEND_CONDITIONS: {
      if (enabling_conditions) {
        next_state = E_WAIT_STARTTIME;
      }
      break;
    }
    case E_WAIT_STARTTIME: {
      if (!enabling_conditions) {
        next_state = E_PEND_CONDITIONS;
      } else if (second_timers[sm_num]==0) {
        next_state = E_ACTIVE;
      }
      break;
    }
    case E_ACTIVE: {
      if (!enabling_conditions) {
        if (Param_RunOutTime[sm_num] == 0) {
          next_state = E_WAIT_MINOFFTIME;
        } else {
          next_state = E_RUNOUT_ACTIVE;
        }
      }
      break;
    }
    case E_RUNOUT_ACTIVE: {
      if (second_timers[sm_num]==0) {
        next_state = E_WAIT_MINOFFTIME;
      }
      break;
    }
    case E_WAIT_MINOFFTIME: {
      if (second_timers[sm_num]==0) {
        next_state = E_PEND_CONDITIONS;
      }
      break;
    }

    default: next_state = E_PEND_CONDITIONS;
  }

  if (  current_state != E_WAIT_STARTTIME
     && next_state    == E_WAIT_STARTTIME
     ) {
    char param_value = Param_StartDelay[sm_num];
    set_timer(sm_num, ((unsigned short)param_value) * 10);
  }
  if (  current_state != E_RUNOUT_ACTIVE
     && next_state    == E_RUNOUT_ACTIVE
     ) {
    char param_value = Param_RunOutTime[sm_num];
    set_timer(sm_num, ((unsigned short)param_value) * 10);
  }
  if (  current_state != E_WAIT_MINOFFTIME
     && next_state    == E_WAIT_MINOFFTIME
     ) {
    char param_value = Param_MinOffTime[sm_num];
    set_timer(sm_num, ((unsigned short)param_value) * 10);
  }

  unsigned char output_pin_num = (unsigned char)Param_OutputPinMap[sm_num];

  if (  next_state == E_ACTIVE
     || next_state == E_RUNOUT_ACTIVE
     // the following term forces duct fans ON if the evaporator is approaching freezing temperatures,
     // and we are forcing the dampers to be open to increase airflow through HVAC.  Having the
     // duct fans running will also increase airflow by applying suction to upstairs
     // supply
     || (  RELAY_ForceDampersOpen
        && (  sm_num == STATE_MACHINE_DUCTFAN_DOWNSTAIRS
           || sm_num == STATE_MACHINE_DUCTFAN_UPSTAIRS
           )
        )
     ) {
    set_bit(porta, output_pin_num);
  } else {
    clear_bit(porta, output_pin_num);
  }

  state_machines[sm_num] = next_state;

}










// CRC working variable
char OneWire_Accum_CRC = 0;
bool AnyNonZeroDataCap = 0;

// CRC lookup table
rom char* crc_rom = {0, 94, 188, 226, 97, 63, 221, 131, 194, 156, 126, 32, 163, 253, 31, 65,
  157, 195, 33, 127, 252, 162, 64, 30, 95, 1, 227, 189, 62, 96, 130, 220,
  35, 125, 159, 193, 66, 28, 254, 160, 225, 191, 93, 3, 128, 222, 60, 98,
  190, 224, 2, 92, 223, 129, 99, 61, 124, 34, 192, 158, 29, 67, 161, 255,
  70, 24, 250, 164, 39, 121, 155, 197, 132, 218, 56, 102, 229, 187, 89, 7,
  219, 133, 103, 57, 186, 228, 6, 88, 25, 71, 165, 251, 120, 38, 196, 154,
  101, 59, 217, 135, 4, 90, 184, 230, 167, 249, 27, 69, 198, 152, 122, 36,
  248, 166, 68, 26, 153, 199, 37, 123, 58, 100, 134, 216, 91, 5, 231, 185,
  140, 210, 48, 110, 237, 179, 81, 15, 78, 16, 242, 172, 47, 113, 147, 205,
  17, 79, 173, 243, 112, 46, 204, 146, 211, 141, 111, 49, 178, 236, 14, 80,
  175, 241, 19, 77, 206, 144, 114, 44, 109, 51, 209, 143, 12, 82, 176, 238,
  50, 108, 142, 208, 83, 13, 239, 177, 240, 174, 76, 18, 145, 207, 45, 115,
  202, 148, 118, 40, 171, 245, 23, 73, 8, 86, 180, 234, 105, 55, 213, 139,
  87, 9, 235, 181, 54, 104, 138, 212, 149, 203, 41, 119, 244, 170, 72, 22,
  233, 183, 85, 11, 136, 214, 52, 106, 43, 117, 151, 201, 74, 20, 246, 168,
  116, 42, 200, 150, 21, 75, 169, 247, 182, 232, 10, 84, 215, 137, 107, 53};




//-----------------------------------------
void OneWire_SendInit() {
//-----------------------------------------
  // Low for 1mSec

  ONEWIREPIN_DRIVE;
  delay_ms(1);
  ONEWIREPIN_RELEASE;

}

//-----------------------------------------
void OneWire_Write(bool data) {
//-----------------------------------------
  clear_bit(intcon, GIE);

  if (data) {
    ONEWIREPIN_DRIVE;
    // 4MHz = 1uSec per instruction
    asm nop;
    asm nop;
    asm nop;
    asm nop;
    asm nop;
    ONEWIREPIN_RELEASE;
  } else {  
    ONEWIREPIN_DRIVE;
    delay_us(65);
    ONEWIREPIN_RELEASE;
  } 
  
  set_bit(intcon, GIE);
}

//-----------------------------------------
void OneWire_WriteByte(unsigned char byteData) {
//-----------------------------------------
  // time in function: 8 * 248 uSec = 1.98 mS
  for (char i=0;i<8;++i) {
    OneWire_Write(byteData & 1);
    asm nop;
    asm nop;
    asm nop;
    asm nop;
    asm nop;
    asm nop;
    asm nop;
    asm nop;
    byteData = byteData >> 1;
  }

}


//-----------------------------------------
bool OneWire_ReadSlot() {
//-----------------------------------------
  bool readData=0;

  clear_bit(intcon, GIE);

  ONEWIREPIN_DRIVE;
  asm nop;
  asm nop;
  asm nop;
  ONEWIREPIN_RELEASE
  asm nop;
  asm nop;
  asm nop;
  asm nop;
  asm nop;
  asm nop;
  asm nop;
  asm nop;
  asm nop;
  if (ONEWIREPIN_TEST) {
    readData = 1;
  }

  set_bit(intcon, GIE);

  delay_us(60);

  return readData;
}

//-----------------------------------------
unsigned char OneWire_ReadByte() {
//-----------------------------------------
  // time in function: 8 * 248 uSec = 1.98 mS

  unsigned char retData=0;

  for (char i=0;i<8;++i) {
    retData = retData >> 1;
    if (OneWire_ReadSlot()) {
      retData |= 0x80;
      AnyNonZeroDataCap=1;
    }
  }

  OneWire_Accum_CRC = crc_rom[OneWire_Accum_CRC ^ retData];

  return retData;
}

//-----------------------------------------
void OneWire_StartConversion() {
  //-----------------------------------------
  //
  // time in function: 6mSec
  // t=0mS
  OneWire_SendInit(); // Low for 1mSec

  delay_ms(1);        // Wait 1mSec
  // t=2mS

  OneWire_WriteByte(0xCC);// Skip ROM
  OneWire_WriteByte(0x44);// Convert
  // t=6mS
}

unsigned int  TempData;

// Scratchpad contents
// 0	Temperature LSB
// 1	Temperature MSB
// 2	Hi alarm temperature
// 3	Lo alarm temperature
// 4	Reserved, 0xFFF
// 5	Reserved, 0xFF
// 6	Remainder register
// 7	Nr of counts per degree
// 8	CRC of pad contents

//-----------------------------------------
void OneWire_ReadConversion() {
//-----------------------------------------
  //
  // time in function: 19mSec

  OneWire_SendInit(); // Low for 1mSec
  delay_ms(1);        // Wait 1mSec

  // t=2mS
  OneWire_WriteByte(0xCC);// Skip ROM
  // t=4mS
  OneWire_WriteByte(0xBE);// Read Scratchpad
  // t=6mS

  OneWire_Accum_CRC = 0;
  AnyNonZeroDataCap = 0;

  TempData  = OneWire_ReadByte();
  TempData |= (OneWire_ReadByte() << 8);
  // t=10mSec

  OneWire_ReadByte(); // hi alarm
  OneWire_ReadByte(); // lo alarm   t = 14mS
  OneWire_ReadByte(); // reserved
  OneWire_ReadByte(); // reserved   t = 16mS
  OneWire_ReadByte(); // remainder
  OneWire_ReadByte(); // counts/deg t = 18mS
  OneWire_ReadByte(); // CRC of PAD

  if (OneWire_Accum_CRC == 0 && AnyNonZeroDataCap) {
    // good CRC
    // analyze
    RxSuctionLineTemp();
  } else {
    BadOrNoSuctionLineTempData();
  }

}

//-----------------------------------------
void BadOrNoSuctionLineTempData() {
//-----------------------------------------
  if (OneWire_ConsecutiveRxFails<3) { // 3 bad readings in a row => NODATA
    ++OneWire_ConsecutiveRxFails;
  } else {
    FreezeDetection_Sense   = E_FD_NODATA;
    OverHeatDetection_Sense = E_FD_NODATA;
  }
}

//-----------------------------------------
void RxSuctionLineTemp() {
//-----------------------------------------
  // This function is called when a good temperature conversion has been performed

  OneWire_ConsecutiveRxFails=0;

  if (TempData & 0x8000) { // negative deg C
    FreezeDetection_Sense = E_FD_BAD2;  
  } 
  else if (TempData <  44) {  // 37F
    FreezeDetection_Sense = E_FD_BAD2;
  }
  else if (TempData <  69) {  // 39.75F
    FreezeDetection_Sense = E_FD_BAD1;
  }
  else if (TempData > 116) {  // 45F
    FreezeDetection_Sense = E_FD_GOOD;
  }
  else {
    FreezeDetection_Sense = E_FD_DEADBAND;
  }
  
  if (TempData & 0x8000) {    // negative deg C
    OverHeatDetection_Sense = E_FD_GOOD;
  }
  else if (TempData > 1050) { // 150F
    OverHeatDetection_Sense = E_FD_BAD1;
  }
  else {
    OverHeatDetection_Sense = E_FD_GOOD;
  }

}

//-----------------------------------------
void OverHeatPreventionStateMachine() {
//-----------------------------------------
  unsigned char current_state = OverHeatPrevention_State;
  unsigned char next_state    = current_state;
  
  switch(current_state) {
    case E_OH_HEAT_OFF_IDLE:
      if (test_bit(input_state, INPUT_HEAT)) {
        next_state = E_OH_HEAT_ON_DETECTION_ACTIVE;
      }
      break;
    case E_OH_HEAT_ON_DETECTION_ACTIVE:
      if (!test_bit(input_state, INPUT_HEAT)) {
        next_state = E_OH_HEAT_OFF_IDLE;
      }
      else if (OverHeatDetection_Sense == E_FD_BAD1) {
        next_state = E_OH_HEAT_ON_DETECTION_PENDING_TRIP;
      }
      break;
    case E_OH_HEAT_ON_DETECTION_PENDING_TRIP:
      if (!test_bit(input_state, INPUT_HEAT)) {
        next_state = E_OH_HEAT_OFF_IDLE;
      }
      else if (OverHeatDetection_Sense != E_FD_BAD1) {
        next_state = E_OH_HEAT_ON_DETECTION_ACTIVE;
      }
      else if (second_timers[SECOND_TIMER_OVERHEAT_PREVENTION]==0) {
        next_state = E_OH_HEAT_TRIP_DWELL;
      }
      break;
    case E_OH_HEAT_TRIP_DWELL:
      if (second_timers[SECOND_TIMER_OVERHEAT_PREVENTION]==0) {
        next_state = E_OH_HEAT_POST_TRIP_2ND_STAGE_INHIBIT;
      }
      break;
    case E_OH_HEAT_POST_TRIP_2ND_STAGE_INHIBIT:
      if (OverHeatDetection_Sense == E_FD_BAD1) {
        next_state = E_OH_HEAT_ON_DETECTION_PENDING_TRIP;
      }
      else if (second_timers[SECOND_TIMER_OVERHEAT_PREVENTION]==0) {
        next_state = E_OH_HEAT_OFF_IDLE;
      }
      break;
    default:
      next_state = E_OH_HEAT_OFF_IDLE;
      break;
  }
  
  if (current_state!=E_OH_HEAT_ON_DETECTION_PENDING_TRIP && next_state==E_OH_HEAT_ON_DETECTION_PENDING_TRIP) {
    set_timer(SECOND_TIMER_OVERHEAT_PREVENTION, 25);   // Wait 25 seconds before tripping heat off for overtemo=p condition (filters out false-high reading)
  }
  if (current_state!=E_OH_HEAT_TRIP_DWELL && next_state==E_OH_HEAT_TRIP_DWELL) {
    set_timer(SECOND_TIMER_OVERHEAT_PREVENTION, 900);  // Keep heat off and dampers open for 15 minutes
  }
  if (current_state!=E_OH_HEAT_POST_TRIP_2ND_STAGE_INHIBIT && next_state==E_OH_HEAT_POST_TRIP_2ND_STAGE_INHIBIT) {
    set_timer(SECOND_TIMER_OVERHEAT_PREVENTION, 3600); // Keep prevent 2nd stage heat from engaging for 1 hour after the trip
  }

  OverHeatPrevention_State = next_state;
}

//-----------------------------------------
void FreezePreventionStateMachine() {
//-----------------------------------------
  unsigned char current_state = FreezePrevention_State;
  unsigned char next_state    = current_state;

  if (  (  !(test_bit(input_state, INPUT_AC))
        && current_state != E_FP_DETECT_BAD_FORCE_AC_OFF  // don't allow the 30 min off-timer to be cleared
        )
     || FreezeDetection_Sense == E_FD_NODATA
     ) {
    next_state = E_FP_AC_OFF_IDLE;
  } else {
    switch (current_state) {
      //---------------------------------------
      case E_FP_AC_OFF_IDLE:
      //---------------------------------------
        if (test_bit(input_state, INPUT_AC)) {
          next_state = E_FP_AC_ON_DETECTION_START_DELAY;
        }
        break;
      //---------------------------------------
      case E_FP_AC_ON_DETECTION_START_DELAY:
      //---------------------------------------
        if (second_timers[SECOND_TIMER_FREEZE_PREVENTION]==0) {
          next_state = E_FP_AC_ON_DETECTION_ACTIVE;
        }
        break;
      //---------------------------------------
      case E_FP_AC_ON_DETECTION_ACTIVE:
      //---------------------------------------
        if (  FreezeDetection_Sense == E_FD_BAD1
           || FreezeDetection_Sense == E_FD_BAD2
           ) {
          next_state = E_FP_AC_ON_DETECT_BAD_ENFORCE_DELAY;
        }
        break;
      //---------------------------------------
      case E_FP_AC_ON_DETECT_BAD_ENFORCE_DELAY:
      //---------------------------------------
        if (  FreezeDetection_Sense != E_FD_BAD1
           && FreezeDetection_Sense != E_FD_BAD2
           ) {
          next_state = E_FP_AC_ON_DETECTION_ACTIVE;
        }
        if (second_timers[SECOND_TIMER_FREEZE_PREVENTION]==0) {
          next_state = E_FP_AC_ON_DETECT_BAD_FORCE_DAMPERS_OPEN;
        }
        break;
      //---------------------------------------
      case E_FP_AC_ON_DETECT_BAD_FORCE_DAMPERS_OPEN:
      //---------------------------------------
        if (  FreezeDetection_Sense == E_FD_BAD2
           && second_timers[SECOND_TIMER_FREEZE_PREVENTION]==0 // we've been in this state for at least 80 seconds
           ) {
          next_state = E_FP_AC_ON_DETECT_BAD_FORCE_DAMPERS_OPEN_WAIT_OFF;
        }
        if (FreezeDetection_Sense == E_FD_GOOD) {
          next_state = E_FP_AC_ON_DETECT_GOOD_RELAX_DELAY;
        }
        break;
      
      //---------------------------------------
      case E_FP_AC_ON_DETECT_BAD_FORCE_DAMPERS_OPEN_WAIT_OFF:
      //---------------------------------------
        if (second_timers[SECOND_TIMER_FREEZE_PREVENTION]==0) { // 20 second timer
          next_state = E_FP_DETECT_BAD_FORCE_AC_OFF;
        }
        if (FreezeDetection_Sense != E_FD_BAD2) {
          next_state = E_FP_AC_ON_DETECT_BAD_FORCE_DAMPERS_OPEN;
        }
        break;
        
      //---------------------------------------
      case E_FP_DETECT_BAD_FORCE_AC_OFF:
      //---------------------------------------
        if (second_timers[SECOND_TIMER_FREEZE_PREVENTION]==0) { // 30 min timer
          next_state = E_FP_AC_ON_DETECTION_ACTIVE;
        }
        break;
      //---------------------------------------
      case E_FP_AC_ON_DETECT_GOOD_RELAX_DELAY:
      //---------------------------------------
        if (second_timers[SECOND_TIMER_FREEZE_PREVENTION]==0) {
          next_state = E_FP_AC_ON_DETECTION_ACTIVE;
        }
        if (FreezeDetection_Sense != E_FD_GOOD) {
          next_state = E_FP_AC_ON_DETECT_BAD_FORCE_DAMPERS_OPEN;
        }
        break;

      default:
        next_state = E_FP_AC_OFF_IDLE; // Fix lost state machine        
    }
  }

  if (current_state != E_FP_AC_ON_DETECTION_START_DELAY && next_state == E_FP_AC_ON_DETECTION_START_DELAY) {
    set_timer(SECOND_TIMER_FREEZE_PREVENTION, 180);   // Allow AC to run for 3 minutes before enforcing the low-temp limit
  }
  if (current_state != E_FP_AC_ON_DETECT_BAD_ENFORCE_DELAY && next_state == E_FP_AC_ON_DETECT_BAD_ENFORCE_DELAY) {
    set_timer(SECOND_TIMER_FREEZE_PREVENTION, 20);    // Wait for 20 seconds of continuous below low temp threshold
  }
  if (current_state == E_FP_AC_ON_DETECT_BAD_ENFORCE_DELAY && next_state == E_FP_AC_ON_DETECT_BAD_FORCE_DAMPERS_OPEN) { // entering FORCE_DAMPERS_OPEN from ENFORCE_DELAY *only*...
                                                      // ...This is because we don't want re-arrival from FORCE_DAMPERS_OPEN_WAIT_OFF to reset this timer
    set_timer(SECOND_TIMER_FREEZE_PREVENTION, 80);    // This gives time for dampers to open before allowing BAD2 temp limit to turn AC off
  }
  if (current_state != E_FP_AC_ON_DETECT_BAD_FORCE_DAMPERS_OPEN_WAIT_OFF && next_state == E_FP_AC_ON_DETECT_BAD_FORCE_DAMPERS_OPEN_WAIT_OFF) {
    set_timer(SECOND_TIMER_FREEZE_PREVENTION, 20);    // Wait for 20 seconds of continuout below low low temp threshold.
                                                      // This allows for a single flaky sensor reading to be ignored
  }
  if (current_state != E_FP_DETECT_BAD_FORCE_AC_OFF && next_state == E_FP_DETECT_BAD_FORCE_AC_OFF) {
    set_timer(SECOND_TIMER_FREEZE_PREVENTION, 1800);  // Keep AC off for 30 minutes to allow coil to melt and warm up
  }
  if (current_state != E_FP_AC_ON_DETECT_GOOD_RELAX_DELAY && next_state == E_FP_AC_ON_DETECT_GOOD_RELAX_DELAY) {
    set_timer(SECOND_TIMER_FREEZE_PREVENTION, 300);   // Wait for 5 minutes of being above low temp threshold (+deadband) before allowing any dampers to close.
  }
  


  FreezePrevention_State = next_state;
}








